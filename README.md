## About ##

### Folders ###
1. Client - front-end React app.
2. Common - shared .NET code. Infrastructure managements, interfaces, etc. Back-end side.
3. External - Integration with some external APIs. Back-end side.
4. Server:
4.1. API - .NET APi layer.  ASP.NET WEB API. Data mapping, serialization, authentication.
4.2. Business - business layer, DI. .NET
4.3. Data - data layer.  Dapper, DAO, stored procedures. .NET
4.4. Infrastructure - DI, App configuration, IoC. .NET


DBMS - MS SQL Server. 
\Server\Data\Migrations - T-SQL initial DDL & DML.