﻿using System;
using System.Threading.Tasks;
using Common.Mailing;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Sendgrid
{
  public class SendgridMailSender : IMailSender
  {
    private readonly SendgridConfig _config;
    public object Config => _config;

    public SendgridMailSender(SendgridConfig config)
    {
      _config = config;
    }

    public async Task SendAsync(string fromAddress, string toAddress, string body, string subject)
    {
      dynamic sg = new SendGridAPIClient(_config.ApiKey);

      var from = new Email(fromAddress);
      var to = new Email(toAddress);
      var content = new Content("text/html", body);
      var mail = new Mail(from, subject, to, content);

      var response = await sg.client.mail.send.post(requestBody: mail.Get());
    }
  }
}
