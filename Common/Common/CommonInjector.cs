﻿using Common.Data.Serializing;
using Common.Mailing;

namespace Common
{
  public static class CommonInjector
  {
    public static ISerializer DefaultSerializer { get; private set; }
    public static IMailSender MailSender { get; private set; }

    public static void Initialize(ISerializer serializer, IMailSender mailSender)
    {
      DefaultSerializer = serializer;
      MailSender = mailSender;
    }
  }
}
