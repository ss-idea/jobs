﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Data.Database
{
  public interface IDaoBase<TEntity>
  {
    Task<List<TEntity>> GetAllAsync();
    Task<List<TEntity>> GetAllAsync(int? page, int? number);
    Task<TEntity> GetByIdAsync<TKey>(TKey id);
  }
}