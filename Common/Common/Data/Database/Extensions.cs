﻿using System.Data;

namespace Common.Data.Database
{
  public static class Extensions
  {
    public static int? GetInt32Nullable(this IDataReader reader, int columnIndex)
    {
      return !reader.IsDBNull(columnIndex) ? reader.GetInt32(columnIndex) : default(int);
    }
  }
}
