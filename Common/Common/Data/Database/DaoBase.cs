﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;

namespace Common.Data.Database
{
  public abstract class DaoBase<TEntity> : IDaoBase<TEntity>
  {
    private readonly string _dbConnectionString;
    public const string TableValuedTypeName = "IdsList";
    public const string TableValuedColumnName = "Id";
    public static string TableName { get; private set; }

    protected DaoBase(string dbConnectionString, string tableName)
    {
      _dbConnectionString = dbConnectionString;
      TableName = tableName;
    }

    protected IDbConnection NewContext() => new SqlConnection(_dbConnectionString);

    public async Task<List<TEntity>> GetAllAsync(int? page, int? number)
    {
      using (var cx = NewContext())
      {
        IEnumerable<TEntity> entities;

        if (!page.HasValue && !number.HasValue)
          entities = await cx.QueryAsync<TEntity>($"SELECT * FROM [dbo].[{TableName}]");
        else
        {
          if (!page.HasValue) page = 0;
          if (!number.HasValue) number = 10000;

          entities = await cx.QueryAsync<TEntity>(
            $"SELECT * FROM [{TableName}] WITH (NOLOCK) ORDER BY[Id] OFFSET @offset ROWS FETCH NEXT @rows ROWS ONLY",
            new
            {
              offset = page * number,
              rows = number
            }
            );
        }

        return entities.AsList();
      }
    }

    public async Task<List<TEntity>> GetAllAsync()
    {
      using (var cx = NewContext())
      {
        var entities = await cx.QueryAsync<TEntity>($"SELECT * FROM [dbo].[{TableName}] WITH (NOLOCK)");
        return entities.AsList();
      }
    }

    public virtual async Task<TEntity> GetByIdAsync<TKey>(TKey id)
    {
      using (var cx = NewContext())
      {
        var record = await cx.QueryFirstOrDefaultAsync<TEntity>(
          $"SELECT * FROM [dbo].[{TableName}] WITH (NOLOCK) WHERE [Id]=@Id",
          new { id }
          );

        return record;
      }
    }

    public DataTable ParseToTableValuedFormat<TValue>(TValue value, string typeName, string columnName, Type columnType)
      where TValue : IEnumerable
    {
      var resultTable = new DataTable(typeName);
      resultTable.Columns.Add(columnName, columnType);

      if (value == null)
        return resultTable;

      foreach (var val in value)
      {
        var row = resultTable.NewRow();
        row[columnName] = val;
        resultTable.Rows.Add(row);
      }

      return resultTable;
    }
  }
}
