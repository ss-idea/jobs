﻿namespace Common.Data.Mapping
{
  public interface IMapper
  {
    TTo Map<TFrom, TTo>(TFrom from);
  }
}
