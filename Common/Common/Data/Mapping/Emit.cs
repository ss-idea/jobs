﻿using EmitMapper;

namespace Common.Data.Mapping
{
  public class Emit : IMapper
  {
    private readonly ObjectMapperManager _manager;

    public Emit()
    {
      _manager = new ObjectMapperManager();
    }

    public TTo Map<TFrom, TTo>(TFrom from)
    {
      var mapper = _manager.GetMapper<TFrom, TTo>();
      var mappedTo = mapper.Map(from);

      return mappedTo;
    }
  }
}
