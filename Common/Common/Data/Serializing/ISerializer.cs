﻿namespace Common.Data.Serializing
{
  public interface ISerializer
  {
    TResult Deserialize<TResult>(string str);
    string Serialize<T>(T obj);
  }
}
