﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Common.Data.Serializing
{
  public class DefaultJsonNetSerializer : ISerializer
  {
    public virtual TResult Deserialize<TResult>(string str)
    {
      TResult obj;
      try
      {
        obj = JsonConvert.DeserializeObject<TResult>(str);
      }
      catch (System.Exception)
      {
        return default(TResult);
      }

      return obj;
    }

    public virtual string Serialize<T>(T obj)
    {
      var str = JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings
      {
        NullValueHandling = NullValueHandling.Ignore,
        ContractResolver = new CamelCasePropertyNamesContractResolver()
      });

      return str;
    }
  }
}
