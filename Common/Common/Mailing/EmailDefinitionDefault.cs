﻿namespace Common.Mailing
{
  public class EmailDefinitionDefault : IEmailDefinition
  {
    public string FromAddress { get; set; }
    public string Subject { get; set; }
    public string TemplatePath { get; set; }
  }
}
