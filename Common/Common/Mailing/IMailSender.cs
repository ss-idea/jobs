﻿using System.Threading.Tasks;

namespace Common.Mailing
{
  public interface IMailSender
  {
    object Config { get; }
    Task SendAsync(string from, string to, string body, string subject);
  }
}
