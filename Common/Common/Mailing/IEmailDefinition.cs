﻿namespace Common.Mailing
{
  public interface IEmailDefinition
  {
    string FromAddress { get; set; }
    string Subject { get; set; }
    string TemplatePath { get; set; }
  }
}
