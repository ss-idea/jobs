﻿using System.IO;
using RazorEngine;
using RazorEngine.Templating;

namespace Common.Mailing.Templates
{
  public class RazorTemplateLoader : IMailTemplateLoader
  {
    public string Load(string tmplPath, string tmplName, object model)
    {
      var body = Engine.Razor.RunCompile(File.ReadAllText(tmplPath), tmplName, model.GetType(), model);
      return body;
    }
  }
}
