﻿namespace Common.Mailing.Templates
{
  public interface IMailTemplateLoader
  {
    string Load(string tmplPath, string tmplName, object model);
  }
}
