﻿<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>
  </configSections>
  <log4net>
    <appender name="All" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <!-- Date - MM.dd.YYYY only without custom text-->
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} %-5level %logger - %message%newline" />
      </layout>
      <filter>
        <filter type="log4net.Filter.LevelMatchFilter">
          <levelToMatch value="DEBUG"/>
        </filter>
      </filter>
    </appender>

    <appender name="RollingFileAppenderError" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\errors\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <!-- Date - MM.dd.YYYY only without custom text-->
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <threshold value="WARN"/>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} [%t] %-5p %c [%x] - %m%n" />
      </layout>
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="WARN" />
        <levelMax value="ERROR" />
      </filter>
      <filter class="log4net.Filter.DenyAllFilter"/>
    </appender>

    <appender name="Warn" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\warn\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <!-- Date - MM.dd.YYYY only without custom text-->
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <threshold value="INFO"/>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} %logger - %message%newline" />
      </layout>
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="WARN" />
        <levelMax value="WARN" />
      </filter>
      <filter class="log4net.Filter.DenyAllFilter"/>
    </appender>

    <appender name="Info" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\info\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <!-- Date - MM.dd.YYYY only without custom text-->
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <threshold value="DEBUG"/>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} %logger - %message%newline" />
      </layout>
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="INFO" />
        <levelMax value="INFO" />
      </filter>
      <filter class="log4net.Filter.DenyAllFilter"/>
    </appender>

    <appender name="EVENTS_Unspec" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\events\unspec\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} - %message%newline" />
      </layout>
      <filter>
        <filter type="log4net.Filter.LevelMatchFilter">
          <levelToMatch value="INFO"/>
        </filter>
      </filter>
    </appender>

    <appender name="EVENTS_InstanceLifecicle" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\events\InstanceLifecicle\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} - %message%newline" />
      </layout>
      <filter>
        <filter type="log4net.Filter.LevelMatchFilter">
          <levelToMatch value="INFO"/>
        </filter>
      </filter>
    </appender>

    <appender name="EVENTS_UserBehaviour" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\events\UserBehaviour\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} - %message%newline" />
      </layout>
      <filter>
        <filter type="log4net.Filter.LevelMatchFilter">
          <levelToMatch value="INFO"/>
        </filter>
      </filter>
    </appender>

    <appender name="EVENTS_UseCaseEvents" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\events\UserEvents\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} - %message%newline" />
      </layout>
      <filter>
        <filter type="log4net.Filter.LevelMatchFilter">
          <levelToMatch value="INFO"/>
        </filter>
      </filter>
    </appender>

    <appender name="EVENTS_ApiEndpointCall" type="log4net.Appender.RollingFileAppender">
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <file value="log\events\ApiEndpointCall\" />
      <datePattern value="dd-MM-HH'.txt'" />
      <staticLogFileName value="false" />
      <appendToFile value="true" />
      <rollingStyle value="Composite" />
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{M/dd/yyyy h:mm:ss} - %message%newline" />
      </layout>
      <filter>
        <filter type="log4net.Filter.LevelMatchFilter">
          <levelToMatch value="INFO"/>
        </filter>
      </filter>
    </appender>

    <logger name="EVENT_Unspec">
      <level value="INFO"/>
      <appender-ref ref="EVENTS_Unspec"/>
    </logger>
    <logger name="EVENT_InstanceLifecicle">
      <level value="INFO"/>
      <appender-ref ref="EVENTS_InstanceLifecicle"/>
    </logger>
    <logger name="EVENT_UserBehaviour">
      <level value="INFO"/>
      <appender-ref ref="EVENTS_UserBehaviour"/>
    </logger>
    <logger name="EVENT_UseCaseEvents">
      <level value="INFO"/>
      <appender-ref ref="EVENTS_UseCaseEvents"/>
    </logger>
    <logger name="EVENT_ApiEndpointCall">
      <level value="INFO"/>
      <appender-ref ref="EVENTS_ApiEndpointCall"/>
    </logger>

    <root>
      <level value="DEBUG" />
      <appender-ref ref="All" />
      <appender-ref ref="Warn" />
      <appender-ref ref="RollingFileAppenderError" />
      <appender-ref ref="Info" />
    </root>

  </log4net>
</configuration>