﻿using System.IO;
using System.Web.Hosting;
using log4net.Config;

namespace Common.Logging
{
  public class Logging
  {
    public static void Configure(string configFileName)
    {
      if (string.IsNullOrEmpty(configFileName))
        configFileName = HostingEnvironment.MapPath("~/bin/Log.config");

      var fi = new FileInfo(configFileName);
      XmlConfigurator.ConfigureAndWatch(fi);
    }
  }
}
