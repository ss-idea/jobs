﻿using System;

namespace Common.Infrastructure.Exceptions
{
  public class ExceptionBase : Exception
  {
    public ExceptionBase()
    { }

    public ExceptionBase(string message)
      : base(message)
    { }
  }
}
