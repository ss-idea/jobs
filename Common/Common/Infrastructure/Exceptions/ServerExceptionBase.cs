﻿using System.Collections.Generic;
using Common.Web.API.Entities;

namespace Common.Infrastructure.Exceptions
{
  public class ServerExceptionBase : ExceptionBase
  {
    public const string ExDataErrorsKey = "ERRORS_LIST";
    public PublicError Error
    {
      set { SetError(value); }
    }

    public ServerExceptionBase()
    { }

    public virtual ServerExceptionBase SetError(PublicError error)
    {
      if (!Data.Contains(ExDataErrorsKey))
      {
        var list = new List<PublicError> { error };
        Data.Add(ExDataErrorsKey, list);

        return this;
      }

      var exist = Data[ExDataErrorsKey] as List<PublicError>;
      if (exist == null)
        exist = new List<PublicError> { error };

      exist.Add(error);

      return this;
    }
  }
}
