﻿using System.Collections.Generic;
using System.Net;
using System.Text;
using Common.Data.Serializing;
using Common.Web.API.Entities;

namespace Common.Web.API
{
  public class ApiErrorResult : JsonResult
  {
    public ApiErrorResult(PublicError errorObj, HttpStatusCode status = HttpStatusCode.BadRequest, ISerializer serializer = null)
      : base(errorObj, status, serializer)
    { }

    public ApiErrorResult(IEnumerable<PublicError> errorsList, HttpStatusCode status = HttpStatusCode.BadRequest, ISerializer serializer = null)
      : base(errorsList, status, serializer)
    { }

    public ApiErrorResult(PublicError errorObj, Encoding encoding, HttpStatusCode status = HttpStatusCode.BadRequest, ISerializer serializer = null)
      : base(errorObj, encoding, status, serializer)
    { }

    public ApiErrorResult(string message, HttpStatusCode status = HttpStatusCode.BadRequest, ISerializer serializer = null)
      : base(new { message = message, error=true }, status, serializer)
    { }

    public ApiErrorResult(HttpStatusCode status = HttpStatusCode.BadRequest, ISerializer serializer = null)
      : base(new { message = string.Empty, error = true }, status, serializer)
    { }
  }
}
