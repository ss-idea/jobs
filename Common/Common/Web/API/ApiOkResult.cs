﻿using Common.Web.API.Entities;

namespace Common.Web.API
{
  public class ApiOkResult : JsonResult
  {
    public ApiOkResult()
      : base(null)
    { }

    public ApiOkResult(ApiOk data)
      : base(data)
    { }

    public ApiOkResult(object apiOkData)
      : base(new ApiOk { Data = apiOkData })
    { }

    public ApiOkResult(string apiOkCode)
      : base(new ApiOk { Code = apiOkCode })
    { }

    public ApiOkResult(string apiOkCode, object apiOkData)
      : base(new ApiOk { Code = apiOkCode, Data = apiOkData })
    { }
  }
}
