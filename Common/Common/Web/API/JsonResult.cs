﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Common.Data.Serializing;

namespace Common.Web.API
{
  public class JsonResult : IHttpActionResult
  {
    protected readonly object _resultObj;
    protected ISerializer _serializer;
    protected readonly HttpStatusCode _status;
    protected readonly Encoding _encoding = Encoding.UTF8;
    public HttpRequestMessage Request { get; set; }
    public string Content { get; set; }

    /// <summary>
    /// Serializes object (using ISerializer if is passed) to JSON string and returns to client as HTTP reponse message
    /// </summary>
    /// <param name="resultObj">Object to serialize into HTTP response</param>
    /// <param name="status">HTTP response status code</param>
    /// <param name="serializer">Concrete inplementation of ISerializer</param>
    public JsonResult(object resultObj, HttpStatusCode status = HttpStatusCode.OK, ISerializer serializer = null)
    {
      _resultObj = resultObj;
      _status = status;

      SetSerializer(serializer);
    }

    public JsonResult(object resultObj, Encoding encoding, HttpStatusCode status = HttpStatusCode.OK, ISerializer serializer = null)
      : this(resultObj, status, serializer)
    {
      _encoding = encoding;
    }

    protected JsonResult(HttpStatusCode status = HttpStatusCode.OK, ISerializer serializer = null)
    {
      SetSerializer(serializer);
    }

    public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
    {
      var response = new HttpResponseMessage(_status)
      {
        RequestMessage = Request
      };

      if (_resultObj != null)
        response.Content = new StringContent(_serializer.Serialize(_resultObj), _encoding);

      return Task.FromResult(response);
    }

    private void SetSerializer(ISerializer serializer)
    {
      _serializer = serializer ?? CommonInjector.DefaultSerializer;
    }
  }
}
