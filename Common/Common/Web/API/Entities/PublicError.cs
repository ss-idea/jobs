﻿using System;
using Newtonsoft.Json;

namespace Common.Web.API.Entities
{
  /// <summary>
  /// Use ONLY for exceptions or errors are not protected for client's browsers.
  /// </summary>
  [Serializable]
  public class PublicError
  {
    public PublicError()
    { }

    /// <param name="type">Int32 or Enum</param>
    /// <param name="code">Int32 or Enum</param>
    public PublicError(object type, object code, string message=null)
    {
      ErrorType = (int)type;
      ErrorCode = (int)code;
      Message = message;
    }

    [JsonProperty("etype")]
    public int ErrorType;
    [JsonProperty("ecode")]
    public int ErrorCode;
    public const bool Error = true;
    public string Message;
  }
}
