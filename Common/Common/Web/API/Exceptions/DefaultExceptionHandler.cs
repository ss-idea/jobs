﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using Common.Infrastructure.Exceptions;
using Common.Web.API.Entities;
using log4net;

namespace Common.Web.API.Exceptions
{
  public class DefaultExceptionHandler : ExceptionHandler
  {
    private static ILog Log = LogManager.GetLogger("DefaultExceptionHandler");
    public static PublicError DefaultUndefinedError;

    public override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
    {
      var ex = context.Exception;

      var errors = ex.Data[ServerExceptionBase.ExDataErrorsKey];
      var errorsList = errors as List<PublicError>;
      if (errors == null || errorsList == null)
      {
        Log.Error(ex.Message, ex);
#if DEBUG
        context.Result = new ApiErrorResult(new PublicError(0, 0, ex.Message + Environment.NewLine + ex.StackTrace));
#else
        context.Result = new ApiErrorResult("Protected unmanaged Exception found. See log files.");
#endif

        return Task.FromResult<object>(null);
      }

      ApiErrorResult errorResult;
      if (errorsList.Count == 1)
        errorResult = new ApiErrorResult(errorsList[0], HttpStatusCode.InternalServerError);
      else
        errorResult = new ApiErrorResult(errorsList, HttpStatusCode.InternalServerError);

      errorResult.Request = context.Request;

      context.Result = errorResult;
      return Task.FromResult<object>(null);
    }
  }
}
