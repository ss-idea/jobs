﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using log4net;

namespace Common.Web.API.Exceptions
{
  public class DefaultExceptionLogger : ExceptionLogger
  {
    private static ILog Log = LogManager.GetLogger("API_Logic_Exception");

    public override Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
    {
      var ex = context.Exception;
      Log.Error(ex.Message, ex);

      return Task.FromResult<object>(null);
    }
  }
}
