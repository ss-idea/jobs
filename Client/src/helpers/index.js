export const keyMirror = obj => {
	if (!obj) return {};

	for (var key in obj) obj[key] = key;

	return obj;
};

/**
 * @param {*string} search - HTTP GET params list serialized into URL-search param. Like: '?x=y&z=s'
 */
export const deserializeUrlGetParams = search => {
	let result = {};

	if (!search || search.length === 0) return {};

	let getParamsList = [search];

	if (search.indexOf('&') > 0) getParamsList = search.split('&');

	let i = 0;
	getParamsList.forEach(p => {
		let spl = p.split('=');
		if (i++ === 0 && spl[0] && spl[0].indexOf('?') === 0)
			spl[0] = spl[0].slice(1);

		result[spl[0]] = spl[1];
	});

	return result;
};
