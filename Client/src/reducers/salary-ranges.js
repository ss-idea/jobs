import {
  Actions
} from 'actions/salary-ranges';

const initial = {
  isFetching: false,
  list: []
};

export default function (state = initial, action) {
  switch (action.type) {
    case Actions.SALARYRS_GET:
      return {
        ...state,
        isFetching: true
      };

    case Actions.SALARYRS_RECEIVE:
      return {
        ...state,
        list: action.payload.data,
        isFetching: false
      }

    default:
      return state;
  }
}