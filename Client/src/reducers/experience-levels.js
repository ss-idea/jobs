import {
  Actions
} from 'actions/experience-levels';

const initial = {
  isFetching: false,
  list: []
};

export default function (state = initial, action) {
  switch (action.type) {
    case Actions.ELEVELS_GET:
      return {
        ...state,
        isFetching: true
      };

    case Actions.ELEVELS_RECEIVE:
      return {
        ...state,
        list: action.payload.data,
        isFetching: false
      }

    default:
      return state;
  }
}