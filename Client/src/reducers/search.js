import {
  Actions
} from 'actions/search';

const initial = {
  "page": 0,
  "number": 10,
  "location": null,
  "categories": [],
  "fulltext": null,
  "experienceLevels": [],
  "salaryRanges": [],
  "tags": []
};

export default function (state = initial, action) {
  switch (action.type) {
    case Actions.SEARCH_UPDATE_ATTRIBUTE:
      {
        const {
          name,
          value
        } = action.payload;

        let next = { ...state
        };
        next[name] = value;

        return next;
      }

    case Actions.SEARCH_ADD_LIST_VAL:
      {
        const {
          attrName,
          value
        } = action.payload;

        let prevState = state[attrName];
        if (typeof prevState === "undefined")
          return state;

        let copy = [...prevState, value];
        return {
          ...state,
          [attrName]: copy
        };
      }


    case Actions.SEARCH_REMOVE_LIST_VAL:
      {
        const {
          attrName,
          value
        } = action.payload;

        let prevState = state[attrName];
        if (typeof prevState === "undefined")
          return state;

        let idx = prevState.indexOf(value);
        if (idx === -1)
          return state;

        let copy = [...prevState];
        copy.splice(idx, 1);

        return {
          ...state,
          [attrName]: copy
        };
      }


    case Actions.SEARCH_DISPOSE_LIST:
      return {
        ...state,
        [action.payload.attrName]: []
      };

    default:
      return state;
  }
}