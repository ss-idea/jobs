import {
	combineReducers
} from 'redux';
import {
	reducer as formReducer
} from 'redux-form';
import jobs from './jobs';
import search from './search';
import experienceLevels from './experience-levels';
import jobCategories from './job-categories';
import salaryRanges from './salary-ranges';
import tags from './tags';

export default combineReducers({
	form: formReducer,
	jobs,
	search,
	experienceLevels,
	jobCategories,
	salaryRanges,
	tags
});