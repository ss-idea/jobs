import {
  Actions
} from 'actions/tags';

const initial = {
  isFetching: false,
  list: []
};

export default function (state = initial, action) {
  switch (action.type) {
    case Actions.TAGS_GET:
      return {
        ...state,
        isFetching: true
      };

    case Actions.TAGS_RECEIVE:
      return {
        ...state,
        list: action.payload.data,
        isFetching: false
      }

    default:
      return state;
  }
}