import {
  Actions
} from 'actions/jobs';
import objectAssign from 'object-assign';

const initial = {
  isFetching: false,
  totalCount: 0,
  list: {
    page0: []
  }
};

export default function (state = initial, action) {
  switch (action.type) {
    case Actions.JOBS_ALL_GET:
      return objectAssign({}, state, {
        isFetching: true
      });

    case Actions.JOBS_ALL_RECEIVE:
      {
        let payload = action.payload;
        if (typeof payload.page === "undefined")
          return state;

        let list = { ...state.list
        };
        list[`page${payload.page}`] = payload.data;

        return {
          ...state,
          list,
          isFetching: false
        }
      }

    case Actions.JOBS_TOTAL_COUNT_RECEIVE:
      return {
        ...state,
        totalCount: action.payload.data.count
      }

    default:
      return state;
  }
}