import {
  Actions
} from 'actions/job-categories';

const initial = {
  isFetching: false,
  list: []
};

export default function (state = initial, action) {
  switch (action.type) {
    case Actions.JCATEGORY_GET:
      return {
        ...state,
        isFetching: true
      };

    case Actions.JCATEGORY_RECEIVE:
      return {
        ...state,
        list: action.payload.data,
        isFetching: false
      }

    default:
      return state;
  }
}