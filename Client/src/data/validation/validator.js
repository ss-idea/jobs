import Joi from 'joi';
import {
	VALIDATION_ERROR
} from 'actions/types';
import {
	SubmissionError
} from 'redux-form';

const validator = {
	validate: (obj, schema, callback) => Joi.validate(obj, schema, callback)
};

export const validate = (values, schema, callback) => {
	//console.log("Validate: ", values);
	let result = validator.validate(values, schema, {
		abortEarly: false
	}, callback);

	if (callback)
		return;

	if (result.error != null) {
		var res = parseToReduxError(result.error);
		//console.log('Errors: ', res);

		res._error = 'Form has errors';
		return res;
	}
};

export const throwIfApiValidationError = action => {
	if (action.type === VALIDATION_ERROR) {
		let errors = parseApiToReduxErrors(action.payload.errors);
		throw new SubmissionError(errors);
	}
};

export const throwIfServerError = (action, statusMessages) => {
	let reduxErrors = {};
	const conflict = 406;
	
	if (action.payload && action.payload.status === conflict) {
		reduxErrors[statusMessages[conflict].fieldTo] = statusMessages[conflict].message;
		throw new SubmissionError(reduxErrors);
	}
}

export const parseApiToReduxErrors = errors => {
	let keys = Object.keys(errors);
	let reduxErrors = {};

	keys.forEach(k => {
		let propName = k;
		if (k === 'email') propName = 'username';

		reduxErrors[propName] = errors[k].message;
	});

	return reduxErrors;
};

export const parseToReduxError = errorDefinition => {
	if (!errorDefinition || !errorDefinition.details) return;

	var resultObj = {};

	errorDefinition.details.forEach(eDetails => {
		resultObj[`${eDetails.context.key}`] = eDetails.message;
	});

	return resultObj;
};