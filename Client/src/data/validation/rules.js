import Joi from 'joi';
import MESSAGES from './messages';


export const rules = {
	email: Joi.string()
		.email()
		.required()
		.options({
			language: MESSAGES.email
		}),

	firstName: Joi.string()
		.regex(
			/^[a-zA-Z]((?![\s]{2})(?![']{2})(?![\.]{2})(?![\-]{2})[a-zA-Z'\s\.\-])*$/
		)
		.min(2)
		.required()
		.options({
			language: MESSAGES.firstName
		}),
	lastName: Joi.string()
		.regex(
			/^[a-zA-Z]((?![\s]{2})(?![']{2})(?![\.]{2})(?![\-]{2})[a-zA-Z'\s\.\-])*$/
		)
		.min(2)
		.required()
		.options({
			language: MESSAGES.lastName
		})
};

export const jobsSearch = Joi.object().keys({
	fulltext: Joi.string().allow('')
});

export const newJob = Joi.object().keys({
	accountEmail: Joi.string().email().required(),
	title: Joi.string().required(),
	location: Joi.string().allow(''),
	jobCategory: Joi.string().required(),
	tags: Joi.array(),
	jobDescription: Joi.string(),
	applicationEmailUrl: Joi.string(),
	closingDate: Joi.string(),
	companyName: Joi.string(),
	companyWebsite: Joi.string(),
	companyDescription: Joi.string(),
	companyLogo: Joi.String()
});