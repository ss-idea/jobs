const _required = 'This is required field';
const _noKey = '';


const _extend = (overrideObj = {
	string: {},
	number: {},
	boolean: {},
	any: {}
}) => ({
	...overrideObj,

	string: {
		min: 'At least {{limit}} chars',
		max: 'Max {{limit}} chars',
		required: _required,
		...overrideObj.string,
	},
	number: {
		base: 'Must be a number',
		positive: 'Must be positive',
		...overrideObj.number,
	},
	boolean: {
		...overrideObj.boolean
	},
	any: {
		empty: _required,
		required: _required,
		...overrideObj.any
	},
	key: _noKey
});

export default {
	email: _extend({
		string: {
			email: 'Must be plain email address'
		}
	}),
	firstName: _extend({
		string: {
			regex: {
				base: 'Must be only latin characters. First is letter.',
				name: 'Must be only latin alphabet, numbers and symbols',
			},
			min: "At least 2 chars"
		},
	}),
	lastName: _extend({
		string: {
			regex: {
				base: 'Must be only latin characters. First is letter.',
				name: 'Must be only latin alphabet, numbers and symbols',
			},
			min: "At least 2 chars"
		},
	})
};