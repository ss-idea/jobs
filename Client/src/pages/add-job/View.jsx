import React from 'react';
import NewJobForm from 'components/forms/NewJob.jsx';
import { Link } from 'react-router-dom';
import routes from 'config/routes';

export default props => (
  <div className="container">
    <div className="sixteen columns">
      <div className="submit-page">

        <NewJobForm />

        <div className="divider margin-top-0"></div>

        <Link to={routes.previewJob.path} className="button big margin-top-5">
          Preview <i className="fa fa-arrow-circle-right"></i>
        </Link>

      </div>
    </div>
  </div>
);