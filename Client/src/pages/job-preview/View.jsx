import React from 'react';

export default props => (
  <div className="container">
    <div className="eleven columns">
      <div className="padding-right">

        <div className="company-info">
          <img src="images/company-logo.png" alt="" />
          <div className="content">
            <h4>King LLC</h4>
            <span><a href="#"><i className="fa fa-link"></i> Website</a></span>
            <span><a href="#"><i className="fa fa-twitter"></i> @kingrestaurants</a></span>
          </div>
          <div className="clearfix"></div>
        </div>

        <p className="margin-reset">
          The Food Service Specialist ensures outstanding customer service is provided to food customers and that all food offerings meet the required stock levels and presentation standards. Beginning your career as a Food Steward will give you a strong foundation in Speedway’s food segment that can make you a vital member of the front line team!
		</p>

        <br />
        <p>The <strong>Food Service Specialist</strong> will have responsibilities that include:</p>

        <ul className="list-1">
          <li>Executing the Food Service program, including preparing and presenting our wonderful food offerings
          to hungry customers on the go!
			</li>
          <li>Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.</li>
          <li>Keeping our Store food area looking terrific and ready for our valued customers by managing product
			inventory, stocking, cleaning, etc.</li>
          <li>We’re looking for associates who enjoy interacting with people and working in a fast-paced environment!</li>
        </ul>

        <br />

        <h4 className="margin-bottom-10">Job Requirment</h4>

        <ul className="list-1">
          <li>Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.</li>
          <li>Must be available to work required shifts including weekends, evenings and holidays.</li>
          <li>Must be able to perform repeated bending, standing and reaching.</li>
          <li> Must be able to occasionally lift up to 50 pounds</li>
        </ul>

      </div>
    </div>



    <div className="five columns">


      <div className="widget">
        <h4>Overview</h4>

        <div className="job-overview">

          <ul>
            <li>
              <i className="fa fa-map-marker"></i>
              <div>
                <strong>Location:</strong>
                <span>20180 Outer Dr Dearborn, MI 48124</span>
              </div>
            </li>
            <li>
              <i className="fa fa-user"></i>
              <div>
                <strong>Job Title:</strong>
                <span>Food Service Specialist</span>
              </div>
            </li>
            <li>
              <i className="fa fa-clock-o"></i>
              <div>
                <strong>Hours:</strong>
                <span>40h / week</span>
              </div>
            </li>
            <li>
              <i className="fa fa-money"></i>
              <div>
                <strong>Rate:</strong>
                <span>$9.50 - $12.50 / hour</span>
              </div>
            </li>
          </ul>


          <a href="#small-dialog" className="popup-with-zoom-anim button">Apply For This Job</a>

          <div id="small-dialog" className="zoom-anim-dialog mfp-hide apply-popup">
            <div className="small-dialog-headline">
              <h2>Apply For This Job</h2>
            </div>

            <div className="small-dialog-content">
              <form action="#" method="get" >
                <input type="text" placeholder="Full Name" value="" />
                <input type="text" placeholder="Email Address" value="" />
                <textarea placeholder="Your message / cover letter sent to the employer"></textarea>

                {/* <!-- Upload CV --> */}
                <div className="upload-info"><strong>Upload your CV (optional)</strong> <span>Max. file size: 5MB</span></div>
                <div className="clearfix"></div>

                <label className="upload-btn">
                  <input type="file" multiple />
                  <i className="fa fa-upload"></i> Browse
							</label>
                <span className="fake-input">No file selected</span>

                <div className="divider"></div>

                <button className="send">Send Application</button>
              </form>
            </div>

          </div>

        </div>

      </div>

    </div>
  </div>
);