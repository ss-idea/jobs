import React from 'react';
import WidgetWrapper from 'components/widgets/WidgetWrapper';
import ExperienceLevelsForm from 'components/forms/SearchExperienceLevels';

export default ({ search, experienceLevels, searchHandlers: { onExperienceLevelChanged } }) => (
  <WidgetWrapper>
    <h4>Experience Level</h4>
    <ExperienceLevelsForm
      levels={experienceLevels}
      onLevelChanged={onExperienceLevelChanged}
      checkedList={search.experienceLevels} />
  </WidgetWrapper>
);