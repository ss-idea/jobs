import React from 'react';
import WidgetWrapper from 'components/widgets/WidgetWrapper';
import SkillsForm from 'components/forms/SearchSkills';

export default ({ search, tags, searchHandlers: { onTagChanged } }) => (
  <WidgetWrapper>
    <h4>Skills</h4>
    <SkillsForm
      skills={tags}
      onSkillChanged={onTagChanged}
      checkedList={search.tags} />
  </WidgetWrapper>
);