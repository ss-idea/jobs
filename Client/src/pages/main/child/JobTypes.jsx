import React, { Component } from 'react';
import PropTypes from 'prop-types';
import WidgetWrapper from 'components/widgets/WidgetWrapper';
import SearchJobTypesForm from 'components/forms/SearchJobTypes';

export default ({ search, jobCategories, searchHandlers: { onCategoryChanged } }) => (
  <WidgetWrapper>
    <h4>Job Type</h4>

    <SearchJobTypesForm
      jobTypes={jobCategories}
      onTypeChanged={onCategoryChanged}
      checkedList={search.categories} />

  </WidgetWrapper>
);