import React from 'react';
import WidgetWrapper from 'components/widgets/WidgetWrapper';
import LocationsSearchForm from 'components/forms/SearchLocation';

export default ({ searchHandlers: { onLocationChanged } }) => (
  <WidgetWrapper>
    <h4>Location</h4>
    <LocationsSearchForm onSubmit={onLocationChanged} />
  </WidgetWrapper>
);