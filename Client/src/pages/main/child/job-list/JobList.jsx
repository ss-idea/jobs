import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import ReactPaginate from 'react-paginate';

class JobList extends Component {
  static propTypes = {
    jobs: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    onPageChanged: PropTypes.func.isRequired
  };

  render() {
    const { jobs, config, onPageChanged } = this.props;
    var pagesNumber = Object.keys(jobs).length;
    var list = jobs[`page${config.page}`];

    if (!list || list.length === 0)
      return null;

    return ([
      <ul key="0" className="job-list full">

        {
          list.map((job, i) => <Item job={job} key={i} />)
        }

      </ul>,
      <div key="1" className="clearfix"></div>,

      <div key="2" className="pagination-container">
        <nav className="pagination">
          {
            pagesNumber > 1
              ? <ReactPaginate
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                breakClassName={"blank"}
                pageCount={pagesNumber}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={onPageChanged}
                activeClassName={"current-page"} />
              : null
          }
        </nav>
      </div>
    ]);
  }
}

export default JobList;