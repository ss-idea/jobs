import React from 'react';
import { Link } from 'react-router-dom';
import routes from 'config/routes';
import numeral from 'numeral';

export default ({ job }) => {
  var salaryType = "-";
  if (job.salaryTypeId === 1)
    salaryType = "hour";
  else if (job.salaryTypeId === 2)
    salaryType = "month";

  return (
    <li>
      <Link
        to={{
          pathname: routes.jobDetails.path(job.contact.companyName.replace(/ /g, "-"), job.title.replace(/ /g, "-")).toLowerCase(),
          search: `?jobId=${job.id}`
        }}>

        <img src={job.contact.companyLogo} alt={`Js Jobs in ${job.contact.companyName}`} />
        <div className="job-list-content">
          <h4>{job.title} <span className="full-time">{job.category.name}</span></h4>
          <div className="job-icons">
            <span><i className="fa fa-briefcase"></i> {job.contact.companyName}</span>
            <span><i className="fa fa-map-marker"></i> {job.location}</span>
            <span><i className="fa fa-money"></i> {job.salaryMin ? `${numeral(job.salaryMin).format('$0,0.00')} - ` : null} {numeral(job.salaryMax).format('$0,0.00')} / {salaryType}</span>
          </div>
          <p dangerouslySetInnerHTML={{ __html: job.description }}></p>
        </div>

      </Link>
      <div className="clearfix"></div>
    </li>
  )
};