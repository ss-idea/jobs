import React from 'react';
import WidgetWrapper from 'components/widgets/WidgetWrapper';
import numeral from 'numeral';
import SalaryRangesForm from 'components/forms/SearchSalaryRanges';

export default ({ search, salaryRanges, searchHandlers: { onSalaryRangeChanged } }) => (
  <WidgetWrapper>
    <h4>Rate / Hr</h4>
    <SalaryRangesForm
      rates={salaryRanges}
      onRateChanged={onSalaryRangeChanged}
      checkedList={search.salaryRanges} />
  </WidgetWrapper>
);