import React from 'react';
import { Link } from 'react-router-dom';

import routes from 'config/routes';
import Locations from './child/Locations';
import JobTypes from './child/JobTypes';
import ExperienceLevels from './child/ExperienceLevels';
import Skills from './child/Skills';
import Rates from './child/Rates';
import JobList from './child/job-list/JobList';
import JobsSearchForm from 'components/forms/JobsSearch';
import Titlebar from 'components/widgets/titlebar/Titlebar';


export default props => {

  const { jobs, jobsTable, onJobsPageChanged, jobsTotal, searchHandlers } = props;

  return ([
    <Titlebar key="0">
      <div className="ten columns">
        <span>We found {jobsTotal} jobs matching:</span>
        <h2>Js Jobs for all js developers</h2>
      </div>

      <div className="six columns">
        <Link to={routes.addJob.path} className="button">
          Post a Job, for $119/60 days
          </Link>
      </div>
    </Titlebar>,

    <div key="1" className="container">
      <div className="five columns">
        <Locations {...props} />
        <JobTypes {...props} />
        <ExperienceLevels {...props} />
        <Rates {...props} />
        <Skills {...props} />
      </div>

      <div className="eleven columns recent-jobs">
        <div className="padding-right">

          <JobsSearchForm onSubmit={searchHandlers.onFulltextSearchChanged} />
          <JobList
            jobs={jobs}
            config={jobsTable}
            onPageChanged={searchHandlers.onPageChanged} />

        </div>
      </div>
    </div>
  ]);
};