import {
  updateAttribute,
  addToListAttribute,
  removeFromListAttribute,
  disposeList
} from 'actions/search';

import {
  search
} from 'actions/jobs';

let _dispatch = null;

export default {
  init: dispatch => {
    _dispatch = dispatch
  },

  onCategoryChanged: (checked, id) => _onAnyCheckListChanged("categories", checked, id),
  onExperienceLevelChanged: (checked, id) => _onAnyCheckListChanged("experienceLevels", checked, id),
  onSalaryRangeChanged:(checked, id) => _onAnyCheckListChanged("salaryRanges", checked, id),
  onTagChanged: (checked, id) => _onAnyCheckListChanged("tags", checked, id),

  onFulltextSearchChanged: nextVal => {
    _dispatch(updateAttribute("fulltext", nextVal.fulltext));
    _search();
  },
  onLocationChanged: nextVal => {
    console.log("Location val: ", nextVal);
    _dispatch(updateAttribute("location", nextVal.location));
    _search();
  },
  onPageChanged: nextPage => {
    console.log("Page val: ", nextPage);
    _dispatch(updateAttribute("page", nextPage));
    _search();
  }
};

const _search = () => {
  _dispatch(search());
}

const _onAnyCheckListChanged = (attributeName, checked, id) => {
  console.log("List check change:", attributeName, checked, id);
  if (id === -1) {
    _dispatch(disposeList(attributeName));
    return _search();
  }

  if (checked)
    _dispatch(addToListAttribute(attributeName, id));
  else
    _dispatch(removeFromListAttribute(attributeName, id));

  _search();
};