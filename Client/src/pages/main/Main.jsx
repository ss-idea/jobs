import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import View from './View';

import * as jobsActions from 'actions/jobs';
import catalogActions from 'actions/catalogs';
import searchHandlers from './search-handlers';

class MainPage extends Component {
  static propTypes = {
    searchHandlers: PropTypes.object.isRequired
  };

  static defaultProps = {
    searchHandlers: searchHandlers
  };

  state = {
    jobsTable: {
      perPage: 10,
      page: 0
    }
  };

  componentWillMount() {
    const { jobsTable: { perPage, page } } = this.state;
    const { dispatch } = this.props;

    dispatch(jobsActions.getTotalCount());
    dispatch(jobsActions.getAll(page, perPage));
    dispatch(catalogActions.jobCategories.getAll());
    dispatch(catalogActions.experienceLevels.getAll());
    dispatch(catalogActions.salaryRanges.getAll());
    dispatch(catalogActions.tags.getAll());
  }

  render() {
    return (
      <View
        {...this.props}
        {...this.state}
      />
    );
  }
}

const mapState = (
  {
    jobs: { list, totalCount },
    experienceLevels,
    jobCategories,
    salaryRanges,
    tags,
    search
  }) => {

  return {
    jobsTotal: totalCount,
    jobs: list,
    experienceLevels: experienceLevels.list || [],
    jobCategories: jobCategories.list || [],
    salaryRanges: salaryRanges.list || [],
    tags: tags.list || [],
    search
  }
};

const mapDispatch = dispatch => {
  searchHandlers.init(dispatch);

  return { dispatch };
};

export default connect(mapState, mapDispatch)(MainPage);