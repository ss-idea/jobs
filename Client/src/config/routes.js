export default {
  main: {
    path: "/"
  },
  addJob: {
    path: "/jobs/post"
  },
  previewJob: {
    path: "/jobs/post/preview"
  },
  jobDetails: {
    path: (companyName = ":companyName", jobTitle = ":jobTitle") => `/jobs/${companyName}/${jobTitle}`
  }
}