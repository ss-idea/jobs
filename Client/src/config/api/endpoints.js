export default {
  jobs: {
    all: "/jobs",
    single: id => `/jobs/${id}`,
    count: "/jobs/count",
    search: "/jobs/search"
  },
  experienceLevels: {
    all: "/experience-levels"
  },
  jobCategories: {
    all: "/job-categories"
  },
  salaryRanges: {
    all: "/salary-ranges"
  },
  tags: {
    all: "/tags"
  },
};