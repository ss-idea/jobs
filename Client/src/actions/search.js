import {
  keyMirror
} from 'helpers';

export const Actions = keyMirror({
  SEARCH_UPDATE_ATTRIBUTE: null,
  SEARCH_ADD_LIST_VAL: null,
  SEARCH_REMOVE_LIST_VAL: null,
  SEARCH_DISPOSE_LIST: null
});

export const updateAttribute = (attrName, attrValue) => dispatch => dispatch({
  type: Actions.SEARCH_UPDATE_ATTRIBUTE,
  payload: {
    name: attrName,
    value: attrValue
  }
});

export const addToListAttribute = (attrName, value) => dispatch => dispatch({
  type: Actions.SEARCH_ADD_LIST_VAL,
  payload: {
    attrName,
    value
  }
});

export const removeFromListAttribute = (attrName, value) => dispatch => dispatch({
  type: Actions.SEARCH_REMOVE_LIST_VAL,
  payload: {
    attrName,
    value
  }
});

export const disposeList = attrName => dispatch => dispatch({
  type: Actions.SEARCH_DISPOSE_LIST,
  payload: {
    attrName
  }
});