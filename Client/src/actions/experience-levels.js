import {
  keyMirror
} from 'helpers';
import {
  request,
  ENDPOINTS
} from './base';


export const Actions = keyMirror({
  ELEVELS_GET: null,
  ELEVELS_RECEIVE: null
});

export const getAll = () => dispatch => {
  // dispatch preloader
  return request.get(ENDPOINTS.experienceLevels.all).then(r => dispatch({
    type: Actions.ELEVELS_RECEIVE,
    payload: r.data
  }))
};