import * as experienceLevels from './experience-levels';
import * as jobCategories from './job-categories';
import * as salaryRanges from './salary-ranges';
import * as tags from './tags';

export default {
  experienceLevels,
  jobCategories,
  salaryRanges,
  tags,
};