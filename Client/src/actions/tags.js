import {
  keyMirror
} from 'helpers';
import {
  request,
  ENDPOINTS
} from './base';


export const Actions = keyMirror({
  TAGS_GET: null,
  TAGS_RECEIVE: null
});

export const getAll = () => dispatch => {
  // dispatch preloader
  return request.get(ENDPOINTS.tags.all).then(r => dispatch({
    type: Actions.TAGS_RECEIVE,
    payload: r.data
  }))
};