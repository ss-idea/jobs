import {
  keyMirror
} from 'helpers';
import {
  request,
  ENDPOINTS
} from './base';


export const Actions = keyMirror({
  SALARYRS_GET: null,
  SALARYRS_RECEIVE: null
});

export const getAll = () => dispatch => {
  // dispatch preloader
  return request.get(ENDPOINTS.salaryRanges.all).then(r => dispatch({
    type: Actions.SALARYRS_RECEIVE,
    payload: r.data
  }))
};