import {
  keyMirror
} from 'helpers';
import {
  request,
  ENDPOINTS,
  dtoMapper
} from './base';


export const Actions = keyMirror({
  JOBS_ALL_GET: null,
  JOBS_ALL_RECEIVE: null,
  JOBS_ALL_ERROR: null,
  JOBS_SINGLE_GET: null,
  JOBS_SINGLE_RECEIVE: null,
  JOBS_SINGLE_ERROR: null,
  JOBS_TOTAL_COUNT_RECEIVE: null
});


export const getAll = (page, number) => dispatch => {
  dispatch({
    type: Actions.JOBS_ALL_GET
  });

  return request.get(`${ENDPOINTS.jobs.all}?page=${page}&number=${number}`)
    .then(r => dispatch({
      type: Actions.JOBS_ALL_RECEIVE,
      payload: {
        page,
        number,
        ...r.data
      },
      error: !r.success
    }));
}

export const getSingle = ({
  jobId
}) => dispatch => {
  dispatch({
    type: Actions.JOBS_SINGLE_GET
  });

  return request.get(ENDPOINTS.jobs.single(jobId))
    .then(r => dispatch({
      type: Actions.JOBS_SINGLE_RECEIVE,
      payload: r.data,
      error: !r.success
    }));
}

export const getTotalCount = () => dispatch => request.get(ENDPOINTS.jobs.count)
  .then(r => dispatch({
    type: Actions.JOBS_TOTAL_COUNT_RECEIVE,
    payload: r.data,
    error: !r.success
  }));


export const search = () => (dispatch, getState) => {
  let state = getState();
  let searchConfig = state.search;
  
  let dto = dtoMapper.map(searchConfig, {
    page: "page",
    number: "number",
    "location": "attributes.location",
    "categories": "attributes.categories",
    "fulltext": "attributes.fulltext",
    "experienceLevels": "attributes.experienceLevels",
    "salaryRanges": "attributes.salaryRanges",
    "tags": "attributes.tags"
  });

  return request.post(ENDPOINTS.jobs.search, dto).then(r => dispatch({
    type: Actions.JOBS_ALL_RECEIVE,
    payload: {
      page: searchConfig.page,
      number: searchConfig.number,
      ...r.data
    },
    error: !r.success
  }));
}