import {
  keyMirror
} from 'helpers';
export const VALIDATION_ERROR = 'VALIDATION_ERROR';



export const API_CALLS = {};

const __prefix = append => `${API_CALLS.prefix}${append}`;

[
  __prefix("JOBS_ALL"),
  __prefix("JOBS_SINGLE"),
].forEach(action => (
  API_CALLS[action] = action
));