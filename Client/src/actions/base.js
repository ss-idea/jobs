import axios from 'axios';
import endpoints from 'config/api/endpoints';
import map from 'object-mapper';

axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.REACT_APP_API_ADDRESS;

export const ENDPOINTS = endpoints;
export const dtoMapper = {
	map: (...args) => map(...args)
};

export let request = axios.create({
	baseURL: process.env.REACT_APP_API_ADDRESS,
	headers: {
		Accept: 'application/json',
		'Content-Type': 'application/json'
	},
	withCredentials: true,
	validateStatus: false
});

export let extendRequest = axios;
export const SERVER_ERRORS = {
	AUTH_REQUIRED: 'auth_req'
};

export const isResponseValid = (response, outPayload) => {
	let status = response.status;

	if (outPayload) {
		if (status === 403) {
			outPayload.error = SERVER_ERRORS.AUTH_REQUIRED;
			return false;
		}
	}

	return status === 200;
};