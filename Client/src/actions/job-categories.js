import {
  keyMirror
} from 'helpers';
import {
  request,
  ENDPOINTS
} from './base';


export const Actions = keyMirror({
  JCATEGORY_GET: null,
  JCATEGORY_RECEIVE: null
});

export const getAll = () => dispatch => {
  // dispatch preloader
  return request.get(ENDPOINTS.jobCategories.all).then(r => dispatch({
    type: Actions.JCATEGORY_RECEIVE,
    payload: r.data
  }))
};