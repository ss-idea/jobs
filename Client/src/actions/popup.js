export const Types = {
  SHOW_POPUP: "SHOW_POPUP",
  HIDE_POPUP: "HIDE_POPUP"
};

export const show = id => dispatch => dispatch({
  type: Types.SHOW_POPUP,
  payload: {
    id
  }
});

export const hide = id => dispatch => dispatch({
  type: Types.HIDE_POPUP,
  payload: {
    id: "hidden"
  }
});
