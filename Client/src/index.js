import 'assets/styles/style.css';
import 'slick-carousel/slick/slick.css';
import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';
import store from './store';
import App from 'components/layout/App';

window.axios = axios;


ReactDOM.render(
	<Provider store={store}>
		<Router>
				<App />
		</Router>
	</Provider>,
	document.getElementById('wrapper')
);
