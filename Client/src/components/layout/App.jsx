import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import routes from 'config/routes';
import Header from 'components/widgets/header/Header';
import Footer from 'components/widgets/footer/Footer';
import BackToTop from 'components/widgets/back-to-top/BackToTop';
import Main from 'pages/main/Main';
import AddJob from 'pages/add-job/AddJob';
import JobPreview from 'pages/job-preview/JobPreview';
import JobDetails from 'pages/job-details/JobDetails';
import POPUPS from 'components/popups';
import { hide as hidePopup } from 'actions/popup';

class App extends Component {

  render() {
    return ([
      <Header key="0" />,
      <div key="1" className="clearfix"></div>,

      <Route
        key="2"
        exact
        path={routes.main.path}
        component={Main}
      />,
      <Route
        key="3"
        exact
        path={routes.addJob.path}
        component={AddJob}
      />,
      <Route
        exact
        key="4"
        path={routes.previewJob.path}
        component={JobPreview}
      />,
      <Route
        exact
        key="5"
        path={routes.jobDetails.path()}
        component={JobDetails}
      />,

      <Footer key="01" />,
      <BackToTop key="02" />
    ]);
  }
}


export default App;
