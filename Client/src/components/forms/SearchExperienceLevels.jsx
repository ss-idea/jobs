import React, { Component } from 'react';
import { container, Checkbox } from './base';

class View extends Component {

  render() {
    const { levels, onLevelChanged, checkedList } = this.props;
    let anyChecked = checkedList.length !== 0;

    return (
      <ul className="checkboxes">
        <li>
          <Checkbox
            checked={anyChecked === false}
            id="check-el-d"
            name="check-el-d"
            component="input"
            onChange={(e, checked) => onLevelChanged(checked, -1)}
            type="checkbox" />
          <label htmlFor="check-el-d">Any Type</label>
        </li>
        {
          levels.map((type, i) => {
            let iName = `check-el-${i}`;
            let checked = false;
            if (anyChecked === true && checkedList.indexOf(type.id) > -1)
              checked = true;

            return (
              <li key={iName}>

                <Checkbox
                  checked={checked}
                  component="input"
                  type="checkbox"
                  onChange={(e, checked) => onLevelChanged(checked, type.id)}
                  id={iName}
                  name={iName}
                  value={type.id} />

                <label htmlFor={iName}>{type.name}</label>
              </li>
            );
          })
        }

      </ul>
    );
  }
}

export default container(View, "JobsSearch_ExperienceLevels");