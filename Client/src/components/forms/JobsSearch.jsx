import React, { Component } from 'react';
import { jobsSearch as validationRule } from 'data/validation/rules';
import { renderField, container, Input } from './base';

class View extends Component {
  handleKeyPress = (e, submit) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      submit();
    }
  }

  render() {
    const { handleSubmit, anyTouched } = this.props;
    return (
      <form onSubmit={handleSubmit} className="list-search">
        <button type="submit">
          <i className="fa fa-search"></i>
        </button>
        <Input
          autoFocus
          name="fulltext"
          type="text"
          placeholder="job title, keywords or company name"
          component="input"
          onKeyPress={(e) => this.handleKeyPress(e, handleSubmit)}
        />
        <div className="clearfix"></div>
      </form>
    );
  }
}

export default container(View, "JobsSearch_Fulltext", validationRule);