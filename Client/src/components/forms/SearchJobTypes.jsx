import React, { Component } from 'react';
import { container, Checkbox } from './base';

class View extends Component {

  render() {
    const { jobTypes, onTypeChanged, checkedList } = this.props;
    let anyChecked = checkedList.length !== 0;

    return (
      <ul className="checkboxes">
        <li>
          <Checkbox
            onChange={onTypeChanged}
            checked={anyChecked === false}
            id="check-jt-d"
            name="check-jt-d"
            component="input"
            onChange={(e, checked) => onTypeChanged(checked, -1)}
            type="checkbox" />
          <label htmlFor="check-jt-d">Any Type</label>
        </li>
        {
          jobTypes.map((type, i) => {
            let iName = `check-jt-${i}`;
            let checked = false;
            if (anyChecked === true && checkedList.indexOf(type.id) > -1)
              checked = true;

            return (
              <li key={iName}>

                <Checkbox
                  checked={checked}
                  component="input"
                  type="checkbox"
                  onChange={(e, checked) => onTypeChanged(checked, type.id)}
                  id={iName}
                  name={iName}
                  value={type.id} />

                <label htmlFor={iName}>{type.name}</label>
              </li>
            );
          })
        }

      </ul>
    );
  }
}

export default container(View, "JobsSearch_Types");