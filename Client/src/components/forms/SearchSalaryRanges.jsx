import React, { Component } from 'react';
import { container, Checkbox } from './base';
import numeral from 'numeral';

class View extends Component {

  render() {
    const { rates, onRateChanged, checkedList } = this.props;
    let anyChecked = checkedList.length !== 0;

    return (
      <ul className="checkboxes">
        <li>
          <Checkbox
            checked={anyChecked === false}
            id="check-sr-d"
            name="check-sr-d"
            component="input"
            onChange={(e, checked) => onRateChanged(checked, -1)}
            type="checkbox" />
          <label htmlFor="check-sr-d">Any Rate</label>
        </li>
        {
          rates.map((type, i) => {
            let iName = `check-sr-${i}`;
            let checked = false;
            if (anyChecked === true && checkedList.indexOf(type.id) > -1)
              checked = true;

            return (
              <li key={iName}>

                <Checkbox
                  checked={checked}
                  component="input"
                  type="checkbox"
                  onChange={(e, checked) => onRateChanged(checked, type.id)}
                  id={iName}
                  name={iName}
                  value={type.id} />

                <label htmlFor={iName}>{numeral(type.min).format('$0,0')}{
                  type.max < 100000 ?
                    ` - ${numeral(type.max).format('$0,0')}`
                    : '+'
                }</label>
              </li>
            );
          })
        }

      </ul>
    );
  }
}

export default container(View, "JobsSearch_SalaryRanges");