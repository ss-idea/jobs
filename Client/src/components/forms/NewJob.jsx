import React from 'react';
import { newJob as validationRule } from 'data/validation/rules';
import { renderField, container, Input } from './base';

const View = ({ handleSubmit, anyTouched }) => (

  <form onSubmit={handleSubmit}>
			<div className="form">
				<h5>Your Email</h5>
				<input className="search-field" type="text" placeholder="mail@example.com" value=""/>
			</div>

			<div className="form">
				<h5>Job Title</h5>
				<input className="search-field" type="text" placeholder="" value=""/>
			</div>

			<div className="form">
				<h5>Location <span>(optional)</span></h5>
				<input className="search-field" type="text" placeholder="e.g. London" value=""/>
				<p className="note">Leave this blank if the location is not important</p>
			</div>

			<div className="form">
				<h5>Job Type</h5>
				<select data-placeholder="Full-Time" className="chosen-select-no-single">
					<option value="1">Full-Time</option>
					<option value="2">Part-Time</option>
					<option value="2">Internship</option>
					<option value="2">Freelance</option>
				</select>
			</div>

			<div className="form">
				<div className="select">
					<h5>Category</h5>
					<select data-placeholder="Choose Categories" className="chosen-select" multiple>
						<option value="1">Web Developers</option>
						<option value="2">Mobile Developers</option>
						<option value="3">Designers & Creatives</option>
						<option value="4">Writers</option>
						<option value="5">Virtual Assistants</option>
						<option value="6">Customer Service Agents</option>
						<option value="7">Sales & Marketing Experts</option>
						<option value="8">Accountants & Consultants</option>
					</select>
				</div>
			</div>

			<div className="form">
				<h5>Job Tags <span>(optional)</span></h5>
				<input className="search-field" type="text" placeholder="e.g. PHP, Social Media, Management" value=""/>
				<p className="note">Comma separate tags, such as required skills or technologies, for this job.</p>
			</div>


			<div className="form">
				<h5>Description</h5>
				<textarea className="WYSIWYG" name="summary" cols="40" rows="3" id="summary"></textarea>
			</div>

			<div className="form">
				<h5>Application email / URL</h5>
				<input type="text" placeholder="Enter an email address or website URL"/>
			</div>

			<div className="form">
				<h5>Closing Date <span>(optional)</span></h5>
				<input data-role="date" type="text" placeholder="yyyy-mm-dd"/>
				<p className="note">Deadline for new applicants.</p>
			</div>

			<div className="divider"><h3>Company Details</h3></div>

			<div className="form">
				<h5>Company Name</h5>
				<input type="text" placeholder="Enter the name of the company"/>
			</div>

			<div className="form">
				<h5>Website <span>(optional)</span></h5>
				<input type="text" placeholder="http://"/>
			</div>

			<div className="form">
				<h5>Tagline <span>(optional)</span></h5>
				<input type="text" placeholder="Briefly describe your company"/>
			</div>

			<div className="form">
				<h5>Video <span>(optional)</span></h5>
				<input type="text" placeholder="A link to a video about your company"/>
			</div>

			<div className="form">
				<h5>Twitter Username <span>(optional)</span></h5>
				<input type="text" placeholder="@yourcompany"/>
			</div>

			<div className="form">
				<h5>Logo <span>(optional)</span></h5>
				<label className="upload-btn">
				    <input type="file" multiple />
				    <i className="fa fa-upload"></i> Browse
				</label>
				<span className="fake-input">No file selected</span>
			</div>

  </form>
);

export default container(View, "NewJob", validationRule);