import React, { Component } from 'react';
import { Field, FieldArray, reduxForm, clearFields } from 'redux-form';
import { validate } from 'data/validation/validator';
import MaskedInput from 'react-text-mask';
import Dropzone from 'react-dropzone';

import store from 'store';

export const Input = Field;
export const Select = Field;
export const FileInput = Field;
export const Checkbox = Field;
export const GroupInputs = FieldArray;

export const container = (Comp, formId, validationRule) => {
	class FormContainer extends Component {
		render() {
			return <Comp {...this.props} />;
		}
	}

	let props = {
		form: formId
	};

	if (validationRule)
		props.validate = v => validate(v, validationRule);

	return reduxForm(props)(FormContainer);
};

export const renderField = field => {
	let {
		input,
		id,
		placeholder,
		label,
		isRequired,
		prepend,
		unit,
    unitClassName,
		disableAdaptivePlaceholder,
		type,
		disabled,
		formatVal,
		formatOptions,
		valueFormatter,
		meta: { touched, error }
	} = field;

	if (disableAdaptivePlaceholder) {
		return <input {...input} type={type} disabled={disabled} />;
	}

	let generalProps = {
		...input,
		id,
		placeholder,
		disabled,
		type,
		className: input.value && 'active'
	};

	if (typeof valueFormatter === "function" && input.value !== '')
		generalProps.value = valueFormatter(input.value);

	let inputComponent = <input {...generalProps} />;

	if (formatVal) {
		inputComponent = (
			<MaskedInput
				keepCharPositions={true}
				guide={false}
				pipe={maskedVal => {
					generalProps.onChange(maskedVal);
					return maskedVal;
				}}
				{...formatOptions}
				{...generalProps}
			/>
		);
	}

	return (
		<div className="inp_container">
			<div className="inp_wrapper">
				{
					prepend
					&& <div className="inp_prepend">
						<span>{prepend}</span>
					</div>
				}

				{inputComponent}

				<label htmlFor={id}>{label}{isRequired ? '*' : null}</label>
				{
					unit
					? <span className={unitClassName}>{unit}</span>
					: null
				}
			</div>
			{touched && error && <p className="warning-msg"> {error}</p>}
		</div>
	);
};

export const renderSelect = ({
	input,
	children,
	id,
	isRequired,
	label,

	meta: { touched, error, warning },
	...props
}) => {
	//console.log(input);
	return (
		<div className="inp_container">
			<div className="inp_wrapper">
				<select {...input} id={id} {...props}>
					{children}
				</select>
				<label htmlFor={id}>{label}{isRequired ? '*' : null}</label>
			</div>
			{touched && error && <p className="warning-msg"> {error}</p>}
		</div>
	);
};

export const renderOptions = (
	options,
	valueProp = 'value',
	labelProp = 'label'
) => {
	return options.map((o, i) => {
		return (
			<option key={i} value={o[valueProp]}>
				{o[labelProp]}
			</option>
		);
	});
};

export const renderFileField = ({
	input: { value: omitValue, onChange, onBlur, name, ...inputProps },
	meta: { touched, error, form },
	fileErrors,
	...props
}) => {
	return (
		<Dropzone
			activeClassName="active"
			disablePreview={true}
			multiple={false}
			name={name}
			onDrop={files => { onChange(files[0]); }}
			className={omitValue ? 'popup_upload_item complete' : 'popup_upload_item'}>
				<div
					onClick={e => {
						e.stopPropagation();

						onChange(undefined);
						store.dispatch(clearFields(form, true, true, name));
					}}
					className="popup_upload_del"
				/>
				<div className="popup_upload_ico" />
				<div className="popup_upload_item_text">{props.label}</div>
				{touched && error && <p className="warning-msg">{error}</p>}
				{fileErrors.imageSize && <p className="warning-msg">{fileErrors.imageSize}</p>}
				<div className="popup_upload_item_instruction">{props.instruction}</div>
		</Dropzone>
	);
};

export const renderCheckbox = field => {
	let {
		input,
		id,
		label,
		meta: { touched, error }
	} = field;

	return (
		<div className="inp_container">
			<div className="inp_check">
				<label htmlFor={id} className="inp_check_label">
					<input {...input} type="checkbox" id={id} />
					<span className="inp_check_icon"></span>
					{label}
				</label>
			</div>
			{touched && error && <p className="warning-msg"> {error}</p>}
		</div>
	);
};