import React, { Component } from 'react';
import { container, Input } from './base';

class View extends Component {
  handleKeyPress = (e, submit) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      submit();
    }
  }

  render() {
    const { handleSubmit, anyTouched } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        
        <Input
          name="location"
          type="text"
          placeholder="State / Province"
          component="input"
          onKeyPress={e => this.handleKeyPress(e, handleSubmit)}
        />

        <button className="button" type="submit">Filter</button>
      </form>
    );
  }
}

export default container(View, "JobsSearch_Location");