import React, { Component } from 'react';
import { container, Checkbox } from './base';

class View extends Component {

  render() {
    const { skills, onSkillChanged, checkedList } = this.props;
    let anyChecked = checkedList.length !== 0;

    return (
      <ul className="checkboxes">
        <li>
          <Checkbox
            checked={anyChecked === false}
            id="check-sk-d"
            name="check-sk-d"
            component="input"
            onChange={(e, checked) => onSkillChanged(checked, -1)}
            type="checkbox" />
          <label htmlFor="check-sk-d">Any Skill</label>
        </li>
        {
          skills.map((type, i) => {
            let iName = `check-sk-${i}`;
            let checked = false;
            if (anyChecked === true && checkedList.indexOf(type.id) > -1)
              checked = true;

            return (
              <li key={iName}>

                <Checkbox
                  checked={checked}
                  component="input"
                  type="checkbox"
                  onChange={(e, checked) => onSkillChanged(checked, type.id)}
                  id={iName}
                  name={iName}
                  value={type.id} />

                <label htmlFor={iName}>{type.title}</label>
              </li>
            );
          })
        }

      </ul>
    );
  }
}

export default container(View, "JobsSearch_Skills");