import React, { Component } from 'react';

class Titlebar extends Component {
  render() {
    return (
      <div id="titlebar">
        <div className="container">
          
          {this.props.children}

        </div>
      </div>
    );
  }
}

export default Titlebar;