import React, { Component } from 'react';

class WidgetWrapper extends Component {
  render() {
    return (
      <div className="widget">
        {this.props.children}
      </div>
    );
  }
}

export default WidgetWrapper;