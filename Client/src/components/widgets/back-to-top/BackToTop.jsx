import React, { Component } from 'react';

class BackToTop extends Component {
  render() {
    return (
      <div id="backtotop"><a href="#"></a></div>
    );
  }
}

export default BackToTop;