import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from 'config/routes';

class Header extends Component {
  render() {
    return (
      <header className="sticky-header">
        <div className="container">
          <div className="sixteen columns">

            <div id="logo">
              <h1>
                <Link to={routes.main.path}>
                  JSHIRES
                </Link>
              </h1>
            </div>

          </div>
        </div>
      </header>
    );
  }
}

export default Header;