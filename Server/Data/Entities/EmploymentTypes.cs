﻿namespace Data.Entities
{
  public enum EmploymentTypes : byte
  {
    Internship = 1,
    Junior,
    Middle,
    Senior
  }
}
