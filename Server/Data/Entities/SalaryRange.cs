﻿namespace Data.Entities
{
  public class SalaryRange
  {
    public int Id { get; set; }
    public double Min { get; set; }
    public double Max { get; set; }
  }
}
