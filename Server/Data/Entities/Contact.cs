﻿using System;

namespace Data.Entities
{
  public class Contact
  {
    public long Id { get; set; }
    public string Email { get; set; }
    public string CompanyName { get; set; }
    public string CompanyLogo { get; set; }
    public string JobTitle { get; set; }
    public string Url { get; set; }
    public DateTime CreationTime { get; set; }
  }
}
