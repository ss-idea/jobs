﻿namespace Data.Entities
{
  public enum SalaryTypes : byte
  {
    Hourly = 1,
    Monthly
  }
}
