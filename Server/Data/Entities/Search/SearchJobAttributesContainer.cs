﻿using System.Collections.Generic;

namespace Data.Entities.Search
{
  public class SearchJobAttributesContainer
  {
    public string Location { get; set; }
    public IEnumerable<short> Categories { get; set; }
    public string Fulltext { get; set; }
    public IEnumerable<ExperienceLevels> ExperienceLevels { get; set; }
    public IEnumerable<int> SalaryRanges { get; set; }
    public IEnumerable<long> Tags { get; set; }
  }
}
