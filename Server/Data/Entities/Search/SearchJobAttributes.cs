﻿namespace Data.Entities.Search
{
  public class SearchJobAttributes : ISearchAttributes<SearchJobAttributesContainer>
  {
    public int Page { get; set; }
    public int Number { get; set; }
    public SearchJobAttributesContainer Attributes { get; set; }
  }
}
