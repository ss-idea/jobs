﻿namespace Data.Entities.Search
{
  public interface ISearchAttributes<TAttrsContainer>
  {
    int Page { get; set; }
    int Number { get; set; }
    TAttrsContainer Attributes { get; set; }
  }
}
