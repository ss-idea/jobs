﻿namespace Data.Entities
{
  public class JobTag
  {
    public long Id { get; set; }
    public long JobId { get; set; }
    public short TagId { get; set; }
  }
}
