﻿namespace Data.Entities
{
  public class Tag
  {
    public short Id { get; set; }
    public string Title { get; set; }
  }
}
