﻿namespace Data.Entities
{
  public enum ExperienceLevels : byte
  {
    Junior = 1,
    Middle,
    Senior,
    Guru
  }
}
