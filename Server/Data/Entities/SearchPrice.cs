﻿namespace Data.Entities
{
  public class SearchPrice
  {
    public double From { get; set; }
    public double To { get; set; }
  }
}
