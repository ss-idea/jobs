﻿using System;

namespace Data.Entities
{
  public class Job
  {
    public long Id { get; set; }
    public long ContactId { get; set; }
    public byte? CategoryId { get; set; }
    public ExperienceLevels? ExperienceId { get; set; }
    public SalaryTypes? SalaryTypeId { get; set; }
    public EmploymentTypes? EmploymentTypeId { get; set; }
    public int? SalaryRangeId { get; set; }
    public DateTime CreationTime { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Location { get; set; }
    public double? SalaryMin { get; set; }
    public double? SalaryMax { get; set; }
    public string InstructionAddress { get; set; }
    public bool IsPaid { get; set; }
  }
}
