﻿namespace Data.Entities
{
  public class JobCategory
  {
    public byte Id { get; set; }
    public string Name { get; set; }
  }
}
