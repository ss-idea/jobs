﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Data.Database;
using Dapper;
using Data.Entities;

namespace Data.DAOs
{
  public class TagsDao : DaoBase<Tag>
  {
    public TagsDao(string dbConnectionString)
      : base(dbConnectionString, "Tags")
    { }

    public async Task<IEnumerable<Tag>> GetByIdAsync(IEnumerable<short> ids)
    {
      using (var cx = NewContext())
      {
        var result = await cx.QueryAsync<Tag>(
          $"SELECT * FROM [{TableName}] WHERE [Id] IN (@Ids)",
          new { ids }
        );

        return result.AsEnumerable();
      }
    }

    public async Task<IEnumerable<Tag>> GetByJobIdAsync(long jobId)
    {
      using (var cx = NewContext())
      {
        var result = await cx.QueryAsync<Tag>(
          $"SELECT * FROM [{TableName}] WHERE [Id] IN (SELECT [TagId] FROM [dbo].[JobTags] WHERE [JobId]=@JobId)",
          new { jobId }
        );

        return result.AsEnumerable();
      }
    }
  }
}
