﻿using System.Threading.Tasks;
using Common.Data.Database;
using Dapper;
using Data.Entities;

namespace Data.DAOs
{
  public class SalaryRangesDao : DaoBase<SalaryRange>
  {
    public SalaryRangesDao(string dbConnectionString)
      : base(dbConnectionString, "SalaryRanges")
    { }

    public async Task<int?> GetRangeIdAsync(double? salaryMin, double? salaryMax)
    {
      using (var cx = NewContext())
      {
        return await cx.ExecuteScalarAsync<int?>(
          $"SELECT [Id] FROM [dbo].[{TableName}] WHERE [Min]>=@Min AND [Max]<=@Max",
          new
          {
            min = salaryMin,
            max = salaryMax
          }
          );
      }
    }
  }
}
