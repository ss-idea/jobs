﻿using System.Threading.Tasks;
using Common.Data.Database;
using Dapper;
using Data.Entities;

namespace Data.DAOs
{
  public class JobCategoriesDao : DaoBase<JobCategory>
  {
    public JobCategoriesDao(string connectionString)
      : base(connectionString, "JobCategories")
    {
    }

    public async Task<JobCategory> GetByJobIdAsync(long jobId)
    {
      using (var cx = NewContext())
      {
        var category = await cx.QueryFirstOrDefaultAsync<JobCategory>(
          $"SELECT * FROM [dbo].[{TableName}] WHERE [Id]=(SELECT [CategoryId] FROM [dbo].[Jobs] WHERE [Id]=@Id)"
          , new { id = jobId }
          );

        return category;
      }
    }
  }
}
