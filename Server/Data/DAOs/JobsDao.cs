﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common.Data.Database;
using Dapper;
using Data.Entities;
using Data.Entities.Search;

namespace Data.DAOs
{
  public class JobsDao : DaoBase<Job>
  {
    public JobsDao(string dbConnectionString)
      : base(dbConnectionString, "Jobs")
    {}

    public async Task<long> AddAsync(Job model)
    {
      using (var cx = NewContext())
      {
        return await cx.ExecuteScalarAsync<long>(
          $"INSERT INTO [dbo].[{TableName}] VALUES (@ContactId, @CategoryId, @ExperienceId, @CreationTime, @Title, @Description, @Location, @SalaryMin, @SalaryMax, @SalaryTypeId, @InstructionAddress, @EmploymentTypeId, @SalaryRangeId, @IsPaid); SELECT CAST(SCOPE_IDENTITY() as bigint)",
          model
          );
      }
    }

    public async Task<int> EditAsync(Job model)
    {
      using (var cx = NewContext())
      {
        return await cx.ExecuteAsync(
          $"UPDATE [dbo].[{TableName}] SET ContactId=@ContactId, CategoryId=@CategoryId, ExperienceId=@ExperienceId, Title=@Title, Description=@Description, Location=@Location, SalaryMin=@SalaryMin, SalaryMax=@SalaryMax, SalaryTypeId=@SalaryTypeId, InstructionAddress=@InstructionAddress, EmploymentTypeId=@EmploymentTypeId, SalaryRangeId=@SalaryRangeId, IsPaid=@IsPaid WHERE Id=@Id",
          model
        );
      }
    }

    public async Task<List<Job>> GetJobsByTagIdAsync(short tagId)
    {
      using (var cx = NewContext())
      {
        var result = await cx.QueryAsync<Job>(
          $"SELECT * FROM [{TableName}] WHERE [Id] IN (SELECT [JobId] FROM [JobTags] WHERE [TagId]=@TagId))",
          new { tagId }
        );

        return result.AsList();
      }
    }

    public async Task<long> GetTotalCountAsync()
    {
      using (var cx = NewContext())
        return await cx.ExecuteScalarAsync<long>(
          $"SELECT Count([Id]) FROM [dbo].[{TableName}]"
        );
    }

    public async Task<IEnumerable<Job>> SearchAsync(SearchJobAttributes attributes)
    {
      return await Task.Run(() =>
      {
        using (var cx = NewContext())
        {
          if (attributes.Page < 0) attributes.Page = 0;
          if (attributes.Number < 1) attributes.Number = 10000;

          var command = cx.CreateCommand();
          command.Parameters.Add(new SqlParameter("@offset", SqlDbType.BigInt)
          {
            Value = attributes.Page * attributes.Number,
            Direction = ParameterDirection.Input
          });
          command.Parameters.Add(new SqlParameter("@number", SqlDbType.BigInt)
          {
            Value = attributes.Number,
            Direction = ParameterDirection.Input
          });
          command.Parameters.Add(new SqlParameter("@location", SqlDbType.NVarChar)
          {
            Value = attributes.Attributes.Location,
            Direction = ParameterDirection.Input
          });
          command.Parameters.Add(new SqlParameter("@fulltext", SqlDbType.NVarChar)
          {
            Value = attributes.Attributes.Fulltext,
            Direction = ParameterDirection.Input
          });
          command.Parameters.Add(new SqlParameter("@categories", SqlDbType.Structured)
          {
            Value = ParseToTableValuedFormat(attributes.Attributes.Categories, TableValuedTypeName, TableValuedColumnName, typeof(long)),
            Direction = ParameterDirection.Input
          });
          command.Parameters.Add(new SqlParameter("@experienceLevels", SqlDbType.Structured)
          {
            Value = ParseToTableValuedFormat(attributes.Attributes.ExperienceLevels, TableValuedTypeName, TableValuedColumnName, typeof(long)),
            Direction = ParameterDirection.Input
          });
          command.Parameters.Add(new SqlParameter("@salaryRanges", SqlDbType.Structured)
          {
            Value = ParseToTableValuedFormat(attributes.Attributes.SalaryRanges, TableValuedTypeName, TableValuedColumnName, typeof(long)),
            Direction = ParameterDirection.Input
          });

          command.Parameters.Add(new SqlParameter("@tags", SqlDbType.Structured)
          {
            Value = ParseToTableValuedFormat(attributes.Attributes.Tags, TableValuedTypeName, TableValuedColumnName, typeof(long)),
            Direction = ParameterDirection.Input
          });

          command.CommandType = CommandType.StoredProcedure;
          command.Connection.Open();
          command.CommandText = "SearchJobs";

          var reader = command.ExecuteReader();

          var result = new List<Job>();
          while (reader.Read())
          {
            result.Add(new Job
            {
              Id = reader.GetInt64(0),
              ContactId = reader.GetInt64(1),
              CategoryId = reader.GetByte(2),
              ExperienceId = (ExperienceLevels)reader.GetByte(3),
              CreationTime = reader.GetDateTime(4),
              Title = reader.GetString(5),
              Description = reader.GetString(6),
              Location = reader.GetString(7),
              SalaryMin = reader.GetDouble(8),
              SalaryMax = reader.GetDouble(9),
              SalaryTypeId = (SalaryTypes)reader.GetByte(10),
              InstructionAddress = reader.GetString(11),
              EmploymentTypeId = (EmploymentTypes)reader.GetByte(12),
              SalaryRangeId = reader.GetInt32Nullable(13),
              IsPaid = reader.GetBoolean(14)
            });
          }

          command.Connection.Dispose();
          return result;
        }
      });
    }
  }
}
