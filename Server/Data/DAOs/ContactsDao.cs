﻿using System.Threading.Tasks;
using Common.Data.Database;
using Dapper;
using Data.Entities;

namespace Data.DAOs
{
  public class ContactsDao : DaoBase<Contact>
  {
    public ContactsDao(string dbConnectionString)
      : base(dbConnectionString, "Contacts")
    { }

    public async Task<long> AddNewAsync(Contact model)
    {
      using (var cx = NewContext())
        return await cx.ExecuteAsync(
          $"INSERT INTO [{TableName}] VALUES(@Email, @CompanyName, @CompanyLogo, @JobTitle, @Url, @CreationTime)",
          model
        );
    }

    public async Task<int> UpdateAsync(Contact model)
    {
      using (var cx = NewContext())
        return await cx.ExecuteAsync(
          $"UPDATE [{TableName}] SET Email=@Email, CompanyName=@CompanyName, CompanyLogo=@CompanyLogo, JobTitle=@JobTitle, Url=@Url WHERE [Id]=@Id",
          model
        );
    }
  }
}
