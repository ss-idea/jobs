﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Data.Database;
using Dapper;
using Data.Entities;

namespace Data.DAOs
{
  public class JobTagsDao : DaoBase<JobTag>
  {
    public JobTagsDao(string dbConnectionString)
      : base(dbConnectionString, "JobTags")
    { }

    public async Task AddListAsync(IEnumerable<JobTag> nextJobTags)
    {
      using (var cx = NewContext())
      {
        await cx.ExecuteAsync(
          $"INSERT INTO [dbo].[{TableName}] VALUES (@JobId, @TagId)",
          nextJobTags
        );
      }
    }

    public async Task<List<JobTag>> GetByJobIdAsync(long jobId)
    {
      using (var cx = NewContext())
      {
        var result = await cx.QueryAsync<JobTag>(
          $"SELECT * FROM [dbo].[{TableName}] WHERE [JobId]=@JobId",
          new { jobId }
        );

        return result.AsList();
      }
    }

    public async Task<int> RemoveListByIdAsync(List<short> tagIdsToRemove, long jobId)
    {
      using (var cx = NewContext())
      {
        return await cx.ExecuteAsync(
          $"DELETE FROM [dbo].[{TableName}] WHERE [JobId]=@JobId AND [TagId] IN @tagIds",
          new
          {
            jobId,
            tagIds = tagIdsToRemove
          }
        );
      }
    }

    public async Task<List<JobTag>> GetByTagIdAsync(short tagId)
    {
      using (var cx = NewContext())
      {
        var result = await cx.QueryAsync<JobTag>(
          $"SELECT * FROM [{TableName}] WHERE [TagId]=@TagId",
          new { tagId }
        );

        return result.AsList();
      }
    }
  }
}
