﻿using System.Threading.Tasks;
using Data.DAOs;
using Data.Entities;

namespace Data.Storages
{
  public class ContactsStorage
  {
    private readonly ContactsDao _dao;

    public ContactsStorage(ContactsDao dao)
    {
      _dao = dao;
    }

    public async Task<long> AddNewAsync(Contact model) => await _dao.AddNewAsync(model);

    public async Task<int> UpdateAsync(Contact model) => await _dao.UpdateAsync(model);

    public async Task<Contact> GetByIdAsync(long id) => await _dao.GetByIdAsync(id);
  }
}
