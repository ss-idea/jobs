﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.DAOs;
using Data.Entities;

namespace Data.Storages
{
  public class SalaryRangesStorage
  {
    private readonly SalaryRangesDao _dao;

    public SalaryRangesStorage(SalaryRangesDao dao)
    {
      _dao = dao;
    }

    public async Task<IEnumerable<SalaryRange>> GetAllRanges() => await _dao.GetAllAsync();

    public async Task<int?> GetRangeIdAsync(double? salaryMin, double? salaryMax)
      => await _dao.GetRangeIdAsync(salaryMin, salaryMax);
  }
}
