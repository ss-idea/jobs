﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.DAOs;
using Data.Entities;

namespace Data.Storages
{
  public class JobCategoriesStorage
  {
    private readonly JobCategoriesDao _dao;

    public JobCategoriesStorage(JobCategoriesDao dao)
    {
      _dao = dao;
    }

    public async Task<List<JobCategory>> GetAllAsync() => await _dao.GetAllAsync();

    public async Task<JobCategory> ByIdAsync(byte id) => await _dao.GetByIdAsync(id);

    public async Task<JobCategory> GetByJobIdAsync(long jobId) => await _dao.GetByJobIdAsync(jobId);
  }
}
