﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.DAOs;
using Data.Entities;

namespace Data.Storages
{
  public class JobTagsStorage
  {
    private readonly JobTagsDao _dao;

    public JobTagsStorage(JobTagsDao dao)
    {
      _dao = dao;
    }

    public async Task AddListAsync(IEnumerable<JobTag> nextJobTags) => await _dao.AddListAsync(nextJobTags);

    public async Task<List<JobTag>> GetByJobIdAsync(long jobId) => await _dao.GetByJobIdAsync(jobId);

    public async Task<int> RemoveListByIdAsync(List<short> tagIdsToRemove, long jobId) => await _dao.RemoveListByIdAsync(tagIdsToRemove, jobId);

    public async Task<List<JobTag>> GetByTagIdAsync(short tagId) => await _dao.GetByTagIdAsync(tagId);
    
  }
}
