﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.DAOs;
using Data.Entities;

namespace Data.Storages
{
  public class TagsStorage
  {
    private readonly TagsDao _dao;

    public TagsStorage(TagsDao dao)
    {
      _dao = dao;
    }

    public async Task<IEnumerable<Tag>> GetAllAsync() => await _dao.GetAllAsync();

    public async Task<IEnumerable<Tag>> GetByIdAsync(IEnumerable<short> ids) => await _dao.GetByIdAsync(ids);

    public async Task<IEnumerable<Tag>> GetByJobIdAsync(long jobId) => await _dao.GetByJobIdAsync(jobId);
  }
}
