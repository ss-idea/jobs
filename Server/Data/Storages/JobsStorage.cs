﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.DAOs;
using Data.Entities;
using Data.Entities.Search;

namespace Data.Storages
{
  public class JobsStorage
  {
    private readonly JobsDao _dao;

    public JobsStorage(JobsDao dao)
    {
      _dao = dao;
    }

    public async Task<List<Job>> GetAllAsync(int? page, int? number) => await _dao.GetAllAsync(page, number);

    public async Task<List<Job>> GetAllAsync() => await _dao.GetAllAsync();

    public async Task<Job> GetByIdAsync(long id) => await _dao.GetByIdAsync(id);

    public async Task<long> AddNewAsync(Job model) => await _dao.AddAsync(model);
    
    public async Task<int> EditAsync(Job next) => await _dao.EditAsync(next);

    public async Task<List<Job>> GetJobsByTagIdAsync(short tagId) => await _dao.GetJobsByTagIdAsync(tagId);

    public async Task<long> GetTotalCountAsync() => await _dao.GetTotalCountAsync();

    public async Task<IEnumerable<Job>> SearchAsync(SearchJobAttributes attributes) => await _dao.SearchAsync(attributes);
  }
}
