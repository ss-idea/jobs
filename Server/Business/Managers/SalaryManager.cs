﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Models;
using Data.Entities;
using Data.Storages;

namespace Business.Managers
{
  public class SalaryManager
  {
    private readonly SalaryRangesStorage _salaryRangesStorage;

    public SalaryManager(SalaryRangesStorage salaryRangesStorage)
    {
      _salaryRangesStorage = salaryRangesStorage;
    }

    public List<SalaryType> GetAll()
    {
      var result = new List<SalaryType>();
      foreach (var typeName in Enum.GetNames(typeof(SalaryTypes)))
        result.Add(ParseSalaryType(typeName));

      return result;
    }

    public SalaryType GetById(SalaryTypes? id)
    {
      if (!id.HasValue)
        return null;

      var value = (SalaryTypes)id;
      return ParseSalaryType(value.ToString());
    }

    public async Task<IEnumerable<SalaryRange>> GetAllRanges() => await _salaryRangesStorage.GetAllRanges();

    public async Task<int?> GetRangeIdAsync(double? salaryMin, double? salaryMax)
      => await _salaryRangesStorage.GetRangeIdAsync(salaryMin, salaryMax);

    #region Helpers

    private SalaryType ParseSalaryType(string name)
    {
      Enum.TryParse(name, true, out SalaryTypes parseResult);
      return new SalaryType
      {
        Id = (byte)parseResult,
        Name = name
      };
    }

    #endregion
  }
}
