﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Models;
using Data.Entities;
using Data.Entities.Search;
using Data.Storages;

namespace Business.Managers
{
  public class JobsManager
  {
    private readonly JobsStorage _storage;
    private readonly JobCategoriesStorage _jobCategoriesStorage;
    private readonly JobTagsStorage _jobTagsStorage;
    private readonly ContactsStorage _contactsStorage;
    private readonly TagsStorage _tagsStorage;
    private readonly SalaryManager _salaryManager;

    public JobsManager(
      JobsStorage storage,
      JobCategoriesStorage jobCategoriesStorage,
      JobTagsStorage jobTagsStorage,
      ContactsStorage contactsStorage,
      TagsStorage tagsStorage,
      SalaryManager salaryManager
      )
    {
      _storage = storage;
      _jobCategoriesStorage = jobCategoriesStorage;
      _jobTagsStorage = jobTagsStorage;
      _contactsStorage = contactsStorage;
      _tagsStorage = tagsStorage;
      _salaryManager = salaryManager;
    }

    public async Task<List<Job>> GetAllAsync() => await _storage.GetAllAsync();

    public async Task<List<JobTag>> GetJobTagsAsync(long jobId) => await _jobTagsStorage.GetByJobIdAsync(jobId);

    public async Task<IEnumerable<Tag>> GetTagsAsync(long jobId) => await _tagsStorage.GetByJobIdAsync(jobId);

    public async Task<JobCategory> GetCategoryAsync(long jobId) => await _jobCategoriesStorage.GetByJobIdAsync(jobId);
    
    public async Task<List<Job>> GetAllAsync(int? page, int? number) => await _storage.GetAllAsync(page, number);
    
    public async Task<Job> GetByIdAsync(long id)=> await _storage.GetByIdAsync(id);
      

    public async Task<long> AddNewAsync(NewJob model)
    {
      model.CreationTime = model.Contact.CreationTime = DateTime.UtcNow;
      var contactId = await _contactsStorage.AddNewAsync(model.Contact);

      model.ContactId = contactId;
      model.SalaryRangeId = await _salaryManager.GetRangeIdAsync(model.SalaryMin, model.SalaryMax);

      var jobId = await _storage.AddNewAsync(model);

      if (model.Tags != null)
      {
        var nextJobTags = new List<JobTag>(model.Tags.Count);
        foreach (var tagId in model.Tags)
        {
          nextJobTags.Add(new JobTag
          {
            JobId = jobId,
            TagId = tagId
          });
        }

        await _jobTagsStorage.AddListAsync(nextJobTags);
      }

      return jobId;
    }

    public async Task<NewJob> EditAsync(NewJob next)
    {
      var prevJob = await _storage.GetByIdAsync(next.Id);
      await _contactsStorage.UpdateAsync(next.Contact);

      if (prevJob == null)
        return null;

      prevJob.CategoryId = next.CategoryId;
      prevJob.ExperienceId = next.ExperienceId;
      prevJob.SalaryTypeId = next.SalaryTypeId;
      prevJob.Title = next.Title;
      prevJob.Description = next.Description;
      prevJob.Location = next.Location;
      prevJob.SalaryMin = next.SalaryMin;
      prevJob.SalaryMax = next.SalaryMax;
      prevJob.SalaryRangeId = await _salaryManager.GetRangeIdAsync(prevJob.SalaryMin, prevJob.SalaryMax);
      prevJob.InstructionAddress = next.InstructionAddress;

      var editedRows = await _storage.EditAsync(prevJob);
      if (editedRows == 0)
        return null;

      if (next.Tags != null)
      {
        List<JobTag> prevTags = await GetJobTagsAsync(prevJob.Id);
        var tagsEditingTasks = new List<Task>();

        if (prevTags == null || prevTags.Count == 0)
        {
          var nextTags = next.Tags.Select(t => new JobTag
          {
            JobId = prevJob.Id,
            TagId = t
          });

          await _jobTagsStorage.AddListAsync(nextTags);
        }
        else
        {
          var tagsToAdd = new List<JobTag>();
          var tagIdsToRemove = new List<short>();

          foreach (var nextTagId in next.Tags)
          {
            if (!prevTags.Exists(t => t.TagId == nextTagId))
              tagsToAdd.Add(new JobTag
              {
                TagId = nextTagId,
                JobId = prevJob.Id
              });

            tagsEditingTasks.Add(_jobTagsStorage.AddListAsync(tagsToAdd));
          }

          foreach (var prevTag in prevTags)
          {
            if (!next.Tags.Contains(prevTag.TagId))
              tagIdsToRemove.Add(prevTag.TagId);

            tagsEditingTasks.Add(_jobTagsStorage.RemoveListByIdAsync(tagIdsToRemove, prevJob.Id));
          }
        }

        if (tagsEditingTasks.Count > 0)
          Task.WaitAll(tagsEditingTasks.ToArray());
      }

      return next;
    }

    public async Task<List<Job>> GetByTagIdAsync(short tagId)
    {
      var jobs = await _storage.GetJobsByTagIdAsync(tagId);
      return jobs;
    }

    public async Task<long> GetTotalCountAsync() => await _storage.GetTotalCountAsync();

    public async Task<IEnumerable<Job>> SearchAsync(SearchJobAttributes attributes)
    {
      var results = await _storage.SearchAsync(attributes);
      return results;
    }
  }
}
