﻿using System;
using System.Collections.Generic;
using Business.Models;
using Data.Entities;

namespace Business.Managers
{
  public class EmploymentManager
  {
    public List<EmploymentType> GetAll()
    {
      var result = new List<EmploymentType>();
      foreach (var typeName in Enum.GetNames(typeof(EmploymentTypes)))
        result.Add(Parse(typeName));

      return result;
    }

    public EmploymentType GetById(EmploymentTypes? id)
    {
      if (!id.HasValue)
        return null;

      var value = (EmploymentTypes)id;
      return Parse(value.ToString());
    }

    private EmploymentType Parse(string name)
    {
      Enum.TryParse(name, true, out EmploymentTypes parseResult);
      return new EmploymentType
      {
        Id = (byte)parseResult,
        Name = name
      };
    }
  }
}
