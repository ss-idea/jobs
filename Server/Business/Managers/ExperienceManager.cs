﻿using System;
using System.Collections.Generic;
using Business.Models;
using Data.Entities;

namespace Business.Managers
{
  public class ExperienceManager
  {
    public List<ExperienceLevel> GetAll()
    {
      var result = new List<ExperienceLevel>();
      foreach (var typeName in Enum.GetNames(typeof(ExperienceLevels)))
        result.Add(Parse(typeName));

      return result;
    }

    public ExperienceLevel GetById(ExperienceLevels? id)
    {
      if (!id.HasValue)
        return null;

      var value = (ExperienceLevels) id;
      return Parse(value.ToString());
    }

    private ExperienceLevel Parse(string name)
    {
      Enum.TryParse(name, true, out ExperienceLevels parseResult);
      return new ExperienceLevel
      {
        Id = (byte)parseResult,
        Name = name
      };
    }
  }
}
