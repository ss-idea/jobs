﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Storages;

namespace Business.Managers
{
  public class JobTagsManager
  {
    private readonly JobTagsStorage _jobTagsStorage;

    public JobTagsManager(JobTagsStorage storage)
    {
      _jobTagsStorage = storage;
    }

    //public async Task<List<short>> GetAllTagIdsAsync(long jobId) => await _jobTagsStorage.GetTagIdsByJobIdAsync(jobId);
  }
}
