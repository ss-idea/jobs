﻿using System.Threading.Tasks;
using System.Web.Hosting;
using Business.Models.Emails;
using Common.Mailing;
using Common.Mailing.Templates;
using Data.Entities;

namespace Business.Managers
{
  public class MailingManager
  {
    private readonly IMailSender _mailSender;
    private readonly IMailTemplateLoader _mailTemplateLoader;

    public MailingManager(IMailSender mailSender, IMailTemplateLoader mailTemplateLoader)
    {
      _mailSender = mailSender;
      _mailTemplateLoader = mailTemplateLoader;
    }

    public async Task SendJobPublished(Contact contact, Job job)
    {
      var viewModel = new JobPublishedEmailModel
      {
        ContactName = contact.CompanyName
      };

      var emailInfo = EmailDefinitions.Info[EmailDefinitions.JobPublishedKey];
      var tmplPath = HostingEnvironment.MapPath(emailInfo.TemplatePath);
      var body = _mailTemplateLoader.Load(tmplPath, EmailDefinitions.JobPublishedKey, viewModel);

      await _mailSender.SendAsync(emailInfo.FromAddress, contact.Email, body, emailInfo.Subject);
    }
  }
}
