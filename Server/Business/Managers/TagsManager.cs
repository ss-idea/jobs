﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Entities;
using Data.Storages;

namespace Business.Managers
{
  public class TagsManager
  {
    private readonly TagsStorage _storage;

    public TagsManager(TagsStorage storage)
    {
      _storage = storage;
    }

    public async Task<IEnumerable<Tag>> GetAllAsync() => await _storage.GetAllAsync();

    public async Task<IEnumerable<Tag>> GetByIdAsync(IEnumerable<short> ids) => await _storage.GetByIdAsync(ids);
  }
}
