﻿using System.Threading.Tasks;
using Data.Entities;
using Data.Storages;

namespace Business.Managers
{
  public class ContactsManager
  {
    private readonly ContactsStorage _contactsStorage;

    public ContactsManager(ContactsStorage storage)
    {
      _contactsStorage = storage;
    }

    public async Task<Contact> GetByIdAsync(long id) => await _contactsStorage.GetByIdAsync(id);
  }
}
