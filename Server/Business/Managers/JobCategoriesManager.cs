﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Entities;
using Data.Storages;

namespace Business.Managers
{
  public class JobCategoriesManager
  {
    private readonly JobCategoriesStorage _storage;

    public JobCategoriesManager(JobCategoriesStorage storage)
    {
      _storage = storage;
    }

    public async Task<List<JobCategory>> GetAllAsync()
    {
      var result = await _storage.GetAllAsync();
      return result;
    }

    public async Task<JobCategory> GetByIdAsync(byte? id)
    {
      if (!id.HasValue)
        return null;

      var result = await _storage.ByIdAsync(id.Value);
      return result;
    }
  }
}
