﻿namespace Business.Models
{
  public class SalaryType
  {
    public byte Id { get; set; }
    public string Name { get; set; }
  }
}
