﻿namespace Business.Models
{
  public class EmploymentType
  {
    public byte Id { get; set; }
    public string Name { get; set; }
  }
}
