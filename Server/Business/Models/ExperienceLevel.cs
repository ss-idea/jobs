﻿namespace Business.Models
{
  public class ExperienceLevel
  {
    public byte Id { get; set; }
    public string Name { get; set; }
  }
}
