﻿using System.Collections.Generic;
using Data.Entities;

namespace Business.Models
{
  public class NewJob : Job
  {
    public List<short> Tags;
    public Contact Contact;
  }
}
