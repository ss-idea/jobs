﻿namespace Business.Models.Emails
{
  public class JobPublishedEmailModel
  {
    public string ContactName { get; set; }
  }
}
