﻿using System;
using System.Collections.Generic;
using Common.Mailing;

namespace Business.Models.Emails
{
  public class EmailDefinitions
  {
    public const string JobPublishedKey = "JobPublished";

    public static Dictionary<string, IEmailDefinition> Info = new Dictionary<string, IEmailDefinition>();

    public static void Initialize(Action<Dictionary<string, IEmailDefinition>> initializer)
    {
      initializer(Info);
    }
  }
}
