﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using Common.Data.Mapping;
using Common.Web.API;
using Common.Web.API.Entities;
using Infrastructure.DI;

namespace Infrastructure.Web
{
  public class ApiControllerBase : ApiController
  {
    protected IMapper DtoMapper;
    private readonly HttpStatusCode DefaultHttpCode = HttpStatusCode.Found;

    protected ApiControllerBase()
    {
      DtoMapper = MappersInjector.Api;
    }

    protected IHttpActionResult BuildOkEmptyResult(string code = null)
    {
      var ok = new ApiOk
      {
        Code = code
      };

      return new ApiOkResult(ok);
    }

    protected IHttpActionResult Ok(object model) => new ApiOkResult(model);
  }
}
