﻿using System;
using Business.Models.Emails;
using Common;
using Common.Data.Mapping;
using Common.Data.Serializing;
using Common.Logging;
using Common.Mailing;
using Common.Web.API.Entities;
using Common.Web.API.Exceptions;
using Infrastructure.DI;
using Sendgrid;

namespace Infrastructure
{
  public class SystemStartInitializer
  {
    public static void Initialize(
      string dbConnectionString,
      string sendgridApiKey,
      TimeSpan jobExpiredIn,
      string loggerConfigFileName = null,
      string corsAllowedDomains = null,
      PublicError undefinedPublicError = null
    )
    {
      Logging.Configure(loggerConfigFileName);
      DefaultExceptionHandler.DefaultUndefinedError = undefinedPublicError;
      AppConfiguration.Initialize(corsAllowedDomains, jobExpiredIn);

      CommonInjector.Initialize(
        new DefaultJsonNetSerializer(),
        new SendgridMailSender(new SendgridConfig {ApiKey = sendgridApiKey})
      );

      // data source
      DaosInjector.Initialize(dbConnectionString);
      MappersInjector.Initialize(new Emit());
    }
  }
}
