﻿using System;

namespace Infrastructure
{
  public static class AppConfiguration
  {
    public static string CorsAllowedDomains { get; private set; }
    public static TimeSpan JobExpiredId { get; private set; }

    public static void Initialize(string corsAllowedDomains, TimeSpan jobExpiredIn)
    {
      CorsAllowedDomains = corsAllowedDomains;
      JobExpiredId = jobExpiredIn;
    }
  }
}
