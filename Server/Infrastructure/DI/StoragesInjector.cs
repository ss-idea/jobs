﻿using Data.Storages;

namespace Infrastructure.DI
{
  public static class StoragesInjector
  {
    public static JobCategoriesStorage JobCategories;
    public static JobsStorage Jobs;
    public static JobTagsStorage JobTags;
    public static TagsStorage Tags;
    public static ContactsStorage Contacts;
    public static SalaryRangesStorage SalaryRanges;

    static StoragesInjector()
    {
      JobCategories = new JobCategoriesStorage(DaosInjector.JobCategories);  
      Jobs  = new JobsStorage(DaosInjector.Jobs);
      JobTags = new JobTagsStorage(DaosInjector.JobTags);
      Tags = new TagsStorage(DaosInjector.Tags);
      Contacts = new ContactsStorage(DaosInjector.Contacts);
      SalaryRanges = new SalaryRangesStorage(DaosInjector.SalaryRanges);
    }
  }
}
