﻿using Business.Managers;
using Common;
using Common.Mailing.Templates;

namespace Infrastructure.DI
{
  public static class ManagersInjector
  {
    public static JobCategoriesManager JobCategories;
    public static JobsManager Jobs;
    public static TagsManager Tags;
    public static ContactsManager Contacts;
    public static ExperienceManager Experience;
    public static SalaryManager Salary;
    public static EmploymentManager Employment;
    public static JobTagsManager JobTags;
    public static MailingManager Mailing;

    static ManagersInjector()
    {
      JobCategories = new JobCategoriesManager(StoragesInjector.JobCategories);
      Tags = new TagsManager(StoragesInjector.Tags);
      Contacts = new ContactsManager(StoragesInjector.Contacts);
      Experience = new ExperienceManager();
      Salary = new SalaryManager(StoragesInjector.SalaryRanges);
      Employment = new EmploymentManager();
      JobTags = new JobTagsManager(StoragesInjector.JobTags);
      Jobs = new JobsManager(
        StoragesInjector.Jobs,
        StoragesInjector.JobCategories,
        StoragesInjector.JobTags,
        StoragesInjector.Contacts,
        StoragesInjector.Tags,
        Salary
      );

      Mailing = new MailingManager(CommonInjector.MailSender, new RazorTemplateLoader());
    }
  }
}
