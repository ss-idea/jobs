﻿using Common.Data.Mapping;

namespace Infrastructure.DI
{
  public class MappersInjector
  {
    public static IMapper DefaultMapper;
    public static IMapper Api;

    public static void Initialize(IMapper defaultMapper)
    {
      DefaultMapper = defaultMapper;
      Api = DefaultMapper;
    }
  }
}
