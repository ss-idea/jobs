﻿using Data.DAOs;

namespace Infrastructure.DI
{
  public static class DaosInjector
  {
    public static JobCategoriesDao JobCategories;
    public static JobsDao Jobs;
    public static JobTagsDao JobTags;
    public static TagsDao Tags;
    public static ContactsDao Contacts;
    public static SalaryRangesDao SalaryRanges;

    public static void Initialize(string msSqlConnectionString)
    {
      JobCategories = new JobCategoriesDao(msSqlConnectionString);
      Jobs = new JobsDao(msSqlConnectionString);
      JobTags =  new JobTagsDao(msSqlConnectionString);
      Tags = new TagsDao(msSqlConnectionString);
      Contacts = new ContactsDao(msSqlConnectionString);
      SalaryRanges = new SalaryRangesDao(msSqlConnectionString);
    }
  }
}
