﻿using System;
using System.Configuration;
using System.Web.Hosting;
using System.Web.Http;
using API.Errors;
using API.Properties;
using Business.Models.Emails;
using Common.Mailing;
using Common.Web.API.Entities;
using Infrastructure;
using log4net;

namespace API
{
  public class WebApiApplication : System.Web.HttpApplication
  {
    private static readonly ILog Log = LogManager.GetLogger("Global.asax");

    protected void Application_Start()
    {
      var appSettings = ConfigurationManager.AppSettings;
      var jobExpiredIn = new TimeSpan(int.Parse(appSettings["JobExpiredInDays"]), 0, 0);

      SystemStartInitializer.Initialize(
        ConfigurationManager.ConnectionStrings["Default"].ConnectionString,
        appSettings["SendgridApiKey"],
        jobExpiredIn,
        appSettings["LoggerConfigFileName"],
        appSettings["corsAllowedDomains"],
        new PublicError(ErrorTypes.Undefined, ErrorCodes.Undefined)
        );

      GlobalConfiguration.Configure(WebApiConfig.Register);
      ConfigureEmails();

      Log.Info("App initialized...");
    }

    private void ConfigureEmails()
    {
      EmailDefinitions.Initialize(container =>
      {
        container.Add(EmailDefinitions.JobPublishedKey, new EmailDefinitionDefault
        {
          FromAddress = ConfigurationManager.AppSettings["MailingFromAddress"],
          Subject = Resources.JobPublishedMailSubject,
          TemplatePath = HostingEnvironment.MapPath(Resources.JobPublishedMailTemplatePath)
        });
      });
    }

    void Application_Error(object sender, EventArgs e)
    {
      var ex = Server.GetLastError();
      Log.Error(ex.Message, ex);
    }
  }
}
