﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using API.DTOs.Response;
using Data.Entities;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/employment-types")]
  public class EmploymentTypesController : ApiControllerBase
  {
    [HttpGet, Route(Order = 0)]
    public IHttpActionResult Get()
    {
      var resultDto = new List<EmploymentTypeDto>();

      foreach (var typeName in Enum.GetNames(typeof(EmploymentTypes)))
      {
        Enum.TryParse(typeName, true, out EmploymentTypes parseResult);
        resultDto.Add(new EmploymentTypeDto
        {
          Id = (byte)parseResult,
          Name = typeName
        });
      }

      return Ok(resultDto);
    }
  }
}