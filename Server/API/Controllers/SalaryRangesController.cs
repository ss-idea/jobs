﻿using System.Collections.Generic;
using System.Web.Http;
using API.DTOs.Response;
using Business.Managers;
using Business.Models;
using Infrastructure.DI;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/experience-levels")]
  public class ExperienceLevelsController : ApiControllerBase
  {
    private readonly ExperienceManager _experienceManager;

    public ExperienceLevelsController()
    {
      _experienceManager = ManagersInjector.Experience;
    }

    [HttpGet, Route(Order = 0)]
    public IHttpActionResult Get()
    {
      var list = _experienceManager.GetAll();
      var resultDto = DtoMapper.Map<List<ExperienceLevel>, List<ExperienceLevelDto>>(list);
      
      return Ok(resultDto);
    }
  }
}