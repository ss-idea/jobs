﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using API.DTOs.Request;
using API.DTOs.Response;
using Business.Managers;
using Business.Models;
using Data.Entities;
using Data.Entities.Search;
using Infrastructure.DI;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/jobs")]
  public class JobsController : ApiControllerBase
  {
    private readonly JobsManager _jobsManager;
    private readonly SalaryManager _salaryManager;
    private readonly ExperienceManager _experienceManager;
    private readonly JobCategoriesManager _jobCategoriesManager;
    private readonly EmploymentManager _employmentManager;
    private readonly ContactsManager _contactsManager;
    private readonly TagsManager _tagsManager;
    private readonly JobTagsManager _jobTagsManager;

    public JobsController()
    {
      _jobsManager = ManagersInjector.Jobs;
      _salaryManager = ManagersInjector.Salary;
      _experienceManager = ManagersInjector.Experience;
      _jobCategoriesManager = ManagersInjector.JobCategories;
      _employmentManager = ManagersInjector.Employment;
      _contactsManager = ManagersInjector.Contacts;
      _tagsManager = ManagersInjector.Tags;
      _jobTagsManager = ManagersInjector.JobTags;
    }

    [HttpGet]
    public async Task<IHttpActionResult> Get()
    {
      var jobs = await _jobsManager.GetAllAsync();
      var responseDto = DtoMapper.Map<List<Job>, List<JobDto>>(jobs);

      foreach (var jobDto in responseDto)
        await InitDependencies(jobDto);

      return Ok(responseDto);
    }

    [HttpGet]
    public async Task<IHttpActionResult> Get(int? page, int? number)
    {
      if (number.HasValue && number.Value == 0)
        number = 1;

      var jobs = await _jobsManager.GetAllAsync(page, number);
      var responseDto = DtoMapper.Map<List<Job>, List<JobDto>>(jobs);

      foreach (var jobDto in responseDto)
        await InitDependencies(jobDto);

      return Ok(responseDto);
    }

    [HttpGet, Route("{id:long}")]
    public async Task<IHttpActionResult> Get(long id)
    {
      var job = await _jobsManager.GetByIdAsync(id);
      var response = DtoMapper.Map<Job, JobDto>(job);

      return Ok(response);
    }


    [HttpGet, Route("count")]
    public async Task<IHttpActionResult> GetCount()
    {
      var count = await _jobsManager.GetTotalCountAsync();
      return Ok(new { count });
    }

    [HttpPut]
    public async Task<IHttpActionResult> Put([FromBody] NewJobDto dto)
    {
      var business = DtoMapper.Map<NewJobDto, NewJob>(dto);
      var nextId = await _jobsManager.AddNewAsync(business);

      return Ok(new { Id = nextId });
    }

    [HttpPost]
    public async Task<IHttpActionResult> Edit([FromBody] NewJobDto dto)
    {
      var business = DtoMapper.Map<NewJobDto, NewJob>(dto);
      var nextModel = await _jobsManager.EditAsync(business);
      var resultDto = DtoMapper.Map<Job, JobDto>(nextModel);

      return Ok(resultDto);
    }

    [HttpPost, Route("search")]
    public async Task<IHttpActionResult> Search([FromBody] SearchJobAttributesDto dto)
    {
      if (dto.Number == 0)
        dto.Number = 1;

      var attributes = DtoMapper.Map<SearchJobAttributesDto, SearchJobAttributes>(dto);
      var results = await _jobsManager.SearchAsync(attributes);

      var resultsDto = DtoMapper.Map<List<Job>, List<JobDto>>(results.ToList());
      if (resultsDto == null)
        return NotFound();

      foreach (var jobDto in resultsDto)
        await InitDependencies(jobDto);

      return Ok(resultsDto);
    }

    private async Task InitDependencies(JobDto jobDto)
    {
      jobDto.Contact = DtoMapper.Map<Contact, ContactDto>(await _contactsManager.GetByIdAsync(jobDto.ContactId));
      jobDto.Category = DtoMapper.Map<JobCategory, JobCategoryDto>(await _jobCategoriesManager.GetByIdAsync(jobDto.CategoryId));
      jobDto.ExperienceLevel = DtoMapper.Map<ExperienceLevel, ExperienceLevelDto>(_experienceManager.GetById(jobDto.ExperienceId));
      jobDto.SalaryType = DtoMapper.Map<SalaryType, SalaryTypeDto>(_salaryManager.GetById(jobDto.SalaryTypeId));
      jobDto.EmploymentType = DtoMapper.Map<EmploymentType, EmploymentTypeDto>(_employmentManager.GetById(jobDto.EmploymentTypeId));
      jobDto.Tags = DtoMapper.Map<IEnumerable<Tag>, IEnumerable<TagDto>>(await _jobsManager.GetTagsAsync(jobDto.Id));
    }
  }
}