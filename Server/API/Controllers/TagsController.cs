﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using API.DTOs.Response;
using Business.Managers;
using Data.Entities;
using Infrastructure.DI;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/tags")]
  public class TagsController : ApiControllerBase
  {
    private readonly TagsManager _manager;
    private readonly JobsManager _jobsManager;

    public TagsController()
    {
      _manager = ManagersInjector.Tags;
      _jobsManager = ManagersInjector.Jobs;
    }

    [HttpGet, Route(Order = 0)]
    public async Task<IHttpActionResult> Get()
    {
      var tagsList = await _manager.GetAllAsync();
      var result = DtoMapper.Map<List<Tag>, List<TagDto>>(tagsList.ToList());

      return Ok(result);
    }

    [HttpGet]
    public async Task<IHttpActionResult> Get([FromUri]IEnumerable<short> ids)
    {
      var tagsList = await _manager.GetByIdAsync(ids);
      var result = DtoMapper.Map<List<Tag>, List<TagDto>>(tagsList.ToList());

      return Ok(result);
    }

    [HttpGet, Route("{id:int}/jobs")]
    public async Task<IHttpActionResult> Get(short id)
    {
      var jobsList = await _jobsManager.GetByTagIdAsync(id);
      var result = DtoMapper.Map<List<Job>, List<JobDto>>(jobsList);

      return Ok(result);
    }
  }
}