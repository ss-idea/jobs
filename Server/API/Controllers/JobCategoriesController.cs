﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using API.DTOs.Response;
using Business.Managers;
using Data.Entities;
using Infrastructure.DI;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/job-categories")]
  public class JobCategoriesController : ApiControllerBase
  {
    private readonly JobCategoriesManager _manager;
    private readonly JobsManager _jobsManager;

    public JobCategoriesController()
    {
      _manager = ManagersInjector.JobCategories;
      _jobsManager = ManagersInjector.Jobs;
    }

    [HttpGet, Route]
    public async Task<IHttpActionResult> Get()
    {
      var allCategories = await _manager.GetAllAsync();
      var responseDto = DtoMapper.Map<List<JobCategory>, List<JobCategoryDto>>(allCategories);
      
      return Ok(responseDto);
    }

    [HttpGet, Route("{id:int}")]
    public async Task<IHttpActionResult> Get(byte id)
    {
      var category = await _manager.GetByIdAsync(id);
      var responseDto = DtoMapper.Map<JobCategory, JobCategoryDto>(category);
      
      return Ok(responseDto);
    }

    [HttpGet, Route("~/api/jobs/{jobId:long}/job-categories")]
    public async Task<IHttpActionResult> GetByJob(long jobId)
    {
      var category = await _jobsManager.GetCategoryAsync(jobId);
      var responseDto = DtoMapper.Map<JobCategory, JobCategoryDto>(category);

      return Ok(responseDto);
    }
  }
}