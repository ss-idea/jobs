﻿using System.Threading.Tasks;
using System.Web.Http;
using API.DTOs.Response;
using Business.Managers;
using Data.Entities;
using Infrastructure.DI;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/contacts")]
  public class ContactsController : ApiControllerBase
  {
    private readonly ContactsManager _contactsManager;

    public ContactsController()
    {
      _contactsManager = ManagersInjector.Contacts;
    }

    [HttpGet, Route("{id:long}")]
    public async Task<IHttpActionResult> Get(long id)
    {
      var contact = await _contactsManager.GetByIdAsync(id);
      var result = DtoMapper.Map<Contact, ContactDto>(contact);

      return Ok(result);
    }
  }
}