﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using API.DTOs.Response;
using Business.Managers;
using Business.Models;
using Data.Entities;
using Infrastructure.DI;
using Infrastructure.Web;

namespace API.Controllers
{
  [RoutePrefix("api/salary-ranges")]
  public class SalaryRangesController : ApiControllerBase
  {
    private readonly SalaryManager _salaryManager;

    public SalaryRangesController()
    {
      _salaryManager = ManagersInjector.Salary;
    }

    [HttpGet, Route(Order = 0)]
    public async Task<IHttpActionResult> Get()
    {
      var list = await _salaryManager.GetAllRanges();
      var resultDto = DtoMapper.Map<List<SalaryRange>, List<SalaryRangeDto>>(list.ToList());
      
      return Ok(resultDto);
    }
  }
}