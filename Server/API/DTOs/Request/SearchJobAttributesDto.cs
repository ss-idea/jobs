﻿using Data.Entities.Search;

namespace API.DTOs.Request
{
  public class SearchJobAttributesDto : ISearchAttributes<SearchJobAttributesContainerDto>
  {
    public int Page { get; set; }
    public int Number { get; set; }
    public SearchJobAttributesContainerDto Attributes { get; set; }
  }
}