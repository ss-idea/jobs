﻿using System.Collections.Generic;
using Data.Entities;

namespace API.DTOs.Request
{
  public class SearchJobAttributesContainerDto
  {
    public string Location { get; set; }
    public IEnumerable<short> Categories { get; set; }
    public string Fulltext { get; set; }
    public IEnumerable<ExperienceLevels> ExperienceLevels { get; set; }
    public IEnumerable<int> SalaryRanges { get; set; }
    public IEnumerable<long> Tags { get; set; }
  }
}