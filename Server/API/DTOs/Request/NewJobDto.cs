﻿using System;
using System.Collections.Generic;
using API.DTOs.Response;

namespace API.DTOs.Request
{
  public class NewJobDto
  {
    public ContactDto Contact;

    public long Id;
    public long ContactId;
    public byte? CategoryId;
    public byte? ExperienceId;
    public byte? SalaryTypeId;
    public DateTime CreationTime;
    public string Title;
    public string Description;
    public string Location;
    public double? SalaryMin;
    public double? SalaryMax;
    public string InstructionAddress;
    public List<short> Tags;
  }
}