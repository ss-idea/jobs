﻿namespace API.DTOs.Response
{
  public class ExperienceLevelDto
  {
    public byte Id;
    public string Name;
  }
}