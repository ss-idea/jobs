﻿namespace API.DTOs.Response
{
  public class SalaryTypeDto
  {
    public byte Id;
    public string Name;
  }
}