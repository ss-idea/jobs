﻿namespace API.DTOs.Response
{
  public class EmploymentTypeDto
  {
    public byte Id;
    public string Name;
  }
}