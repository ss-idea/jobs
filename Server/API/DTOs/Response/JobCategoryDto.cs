﻿namespace API.DTOs.Response
{
  public class JobCategoryDto
  {
    public byte Id;
    public string Name;
  }
}