﻿using System.Collections.Generic;
using Data.Entities;

namespace API.DTOs.Response
{
  public class JobDto : Job
  {
    public ContactDto Contact;
    public JobCategoryDto Category;
    public ExperienceLevelDto ExperienceLevel;
    public SalaryTypeDto SalaryType;
    public EmploymentTypeDto EmploymentType;
    public IEnumerable<TagDto> Tags;
  }
}