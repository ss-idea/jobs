﻿using System.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using Common.Web.API.Exceptions;

namespace API
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Exceptions management
      config.Services.Replace(typeof(IExceptionHandler), new DefaultExceptionHandler());
      config.Services.Replace(typeof(IExceptionLogger), new DefaultExceptionLogger());

      var cors = new EnableCorsAttribute(ConfigurationManager.AppSettings["corsAllowedDomains"], "*", "*")
      {
        SupportsCredentials = true
      };

      config.EnableCors(cors);

      // Web API routes
      config.MapHttpAttributeRoutes();
      config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
    }
  }
}
